import cn.hdc.ch5.pojo.Student;
import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import org.junit.Test;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;

public class StudentTest {
    @Test
    public void studentFindBySid() throws IOException {
//1.获取核心配置文件
        InputStream inputStream = Resources.getResourceAsStream("mybatis-config.xml");
        //2.创建sqlSessionFactory工厂对象
        SqlSessionFactory sqlSessionFactory = new SqlSessionFactoryBuilder().build(inputStream);
        //3.创建sqlSession对象
        SqlSession session = sqlSessionFactory.openSession();
        //4.执行sql语句,sql语句的唯一标识：namespace.statementId
        Student student = session.selectOne("cn.hdc.ch5.pojo.Student.findById", 2);
        System.out.println(student);
        //5.释放资源
        session.close();
    }

    @Test
    public void allStudentFind() throws IOException {
        InputStream inputStream = Resources.getResourceAsStream("mybatis-config.xml");
        //2.创建sqlSessionFactory工厂对象
        SqlSessionFactory sqlSessionFactory = new SqlSessionFactoryBuilder().build(inputStream);
        //3.创建sqlSession对象
        SqlSession session = sqlSessionFactory.openSession();
        //4.执行sql语句,sql语句的唯一标识：namespace.statementId
        List<Student> students = session.selectList("cn.hdc.ch5.pojo.Student.findAll");
        System.out.println(students);
        //5.释放资源
        session.close();
    }

    @Test
    public void addStudent() throws IOException {
        InputStream inputStream = Resources.getResourceAsStream("mybatis-config.xml");
        //2.创建sqlSessionFactory工厂对象
        SqlSessionFactory sqlSessionFactory = new SqlSessionFactoryBuilder().build(inputStream);
        //3.创建sqlSession对象
        SqlSession session = sqlSessionFactory.openSession();
        Student student = new Student();
        student.setSname("zhangsan");
        student.setAge(23);
        student.setCourse("Java");
        student.setSid(7);
        //4.执行sql语句,sql语句的唯一标识：namespace.statementId
        int ret = session.insert("cn.hdc.ch5.pojo.Student.insertOne", student);
        if (ret > 0) {
            System.out.println("插入成功！");
        } else {
            System.out.println("插入失败！");
        }
        //5.释放资源
        session.commit();
        session.close();
    }

    @Test
    public void updateStudent() throws IOException {
        InputStream inputStream = Resources.getResourceAsStream("mybatis-config.xml");
        //2.创建sqlSessionFactory工厂对象
        SqlSessionFactory sqlSessionFactory = new SqlSessionFactoryBuilder().build(inputStream);
        //3.创建sqlSession对象
        SqlSession session = sqlSessionFactory.openSession();
        int ret = session.update("cn.hdc.ch5.pojo.Student.updateOne", 7);
        if (ret > 0) {
            System.out.println("更新成功！");
        } else {
            System.out.println("更新失败！");
        }
        //5.释放资源
        session.commit();
        session.close();
    }

    @Test
    public void deleteStudent() throws IOException {
        InputStream inputStream = Resources.getResourceAsStream("mybatis-config.xml");
        //2.创建sqlSessionFactory工厂对象
        SqlSessionFactory sqlSessionFactory = new SqlSessionFactoryBuilder().build(inputStream);
        //3.创建sqlSession对象
        SqlSession session = sqlSessionFactory.openSession();
        int ret = session.update("cn.hdc.ch5.pojo.Student.deleteOne", 7);
        if (ret > 0) {
            System.out.println("删除成功！");
        } else {
            System.out.println("删除失败！");
        }
        //5.释放资源
        session.commit();
        session.close();
    }
}
