package cn.itsource.day04.mapper;

import cn.itsource.day04.entity.Teacher;

import java.math.BigInteger;
import java.util.List;

public interface TeacherMapper {


    List<Teacher> queryList();

    Teacher queryOne(BigInteger id);

    Integer saveOne(Teacher teacher);

    Integer updateOne(Teacher teacher);

    Integer delOne(BigInteger id);


}
