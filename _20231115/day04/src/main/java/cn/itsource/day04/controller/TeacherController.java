package cn.itsource.day04.controller;

import cn.itsource.day04.entity.Teacher;
import cn.itsource.day04.service.IteacherService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.math.BigInteger;
import java.util.List;


@RestController
@RequestMapping("/teacher")
public class TeacherController {

    @Autowired
    private IteacherService service;


    @GetMapping("/list")
    public List<Teacher> queryList() {
        return service.queryList();
    }


    @GetMapping("/getOne")
    public Teacher queryOne(BigInteger id) {
        return service.queryOne(id);
    }


    @PostMapping("/saveOne")
    public Integer saveOne(Teacher teacher) {
        return service.saveOne(teacher);
    }


    @PutMapping("/editOne")
    public Integer updateOne(Teacher teacher) {
        return service.updateOne(teacher);
    }


    @DeleteMapping("/delOne")
    public Integer delOne(BigInteger id) {
        return service.delOne(id);
    }
}
