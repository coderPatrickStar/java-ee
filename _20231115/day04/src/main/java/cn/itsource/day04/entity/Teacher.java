package cn.itsource.day04.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigInteger;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Teacher {
    private BigInteger id;
    private String name;
    private Integer age;
    private String sex;
    private String position;
    private String subject;
}
