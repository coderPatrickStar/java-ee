package cn.itsource.day04.service.impl;

import cn.itsource.day04.entity.Teacher;
import cn.itsource.day04.mapper.TeacherMapper;
import cn.itsource.day04.service.IteacherService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigInteger;
import java.util.List;


@Service
public class IteacherServiceImpl implements IteacherService {

    @Autowired
    private TeacherMapper mapper;

    @Override
    public List<Teacher> queryList() {
        return mapper.queryList();
    }


    @Override
    public Teacher queryOne(BigInteger id) {
        return mapper.queryOne(id);
    }

    @Override
    public Integer saveOne(Teacher teacher) {
        return mapper.saveOne(teacher);
    }

    @Override
    public Integer updateOne(Teacher teacher) {
        return mapper.updateOne(teacher);
    }

    @Override
    public Integer delOne(BigInteger id) {
        return mapper.delOne(id);
    }
}
