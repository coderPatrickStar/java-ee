package cn.itsource.day05.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigInteger;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Teacher {
    /**
     * id
     * int Integer BigInteger
     */
    private BigInteger id;
    /**
     * 姓名
     */
    private String name;
    /**
     * 年龄
     */
    private Integer age;
    /**
     * 性别
     */
    private String sex;
    /**
     * 职位
     */
    private String position;
    /**
     * 学科
     */
    private String subject;
}
