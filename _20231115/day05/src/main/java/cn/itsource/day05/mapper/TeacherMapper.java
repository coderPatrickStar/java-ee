package cn.itsource.day05.mapper;

import cn.itsource.day05.entity.Teacher;
import org.apache.ibatis.annotations.Mapper;

import java.math.BigInteger;
import java.util.List;


@Mapper
public interface TeacherMapper {
    List<Teacher> queryList();

    Teacher queryOne(BigInteger id);
    Integer saveOne(Teacher teacher);
    Integer updateOne(Teacher teacher);
    Integer delOne(BigInteger id);
}
