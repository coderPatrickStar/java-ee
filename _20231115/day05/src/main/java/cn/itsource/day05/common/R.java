package cn.itsource.day05.common;



public class R<T> {

    /**
     * 状态码
     */
    private Integer code;

    /**
     * 描述
     */
    private String msg;

    /**
     * 拓展数据
     */
    private T data;

    /**
     * 构造器私有：防止别的类误创建结果类
     */
    private R(){}


    /**
     * 初始化当前结果对象
     * @return
     */
    private static  R newInstance(Integer code, String msg){
        R r = new R();
        return r.setCode(code).setMsg(msg);
    }

    /**
     * 响应成功标准返回方法
     * @return
     */
    public static R ok(){
        return newInstance(EcommonR.OK.getCode(), EcommonR.OK.getMsg());
    }

    /**
     * 响应未找到标准返回方法
     * @return
     */
    public static R notfound(){
        return newInstance(EcommonR.NOTFOUND.getCode(), EcommonR.NOTFOUND.getMsg());
    }
    /**
     * 响应成功标准返回方法
     * @return
     */
    public static R error(){
        return newInstance(EcommonR.ERROR.getCode(), EcommonR.ERROR.getMsg());
    }

    public R setCode(Integer code) {
        this.code = code;
        return this;
    }

    public R setMsg(String msg) {
        this.msg = msg;
        return this;
    }

    public R setData(T data) {
        this.data = data;
        return this;
    }


    public Integer getCode() {
        return code;
    }

    public String getMsg() {
        return msg;
    }

    public T getData() {
        return data;
    }
}
