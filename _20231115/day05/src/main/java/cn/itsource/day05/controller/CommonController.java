package cn.itsource.day05.controller;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import cn.itsource.day05.common.R;

import java.io.File;
import java.io.IOException;

@RestController
@RequestMapping("/common")
public class CommonController {

    @Value("${file.upload.path}")
    private String filePath;

    /**
     * 文件上传
     *
     * @param file
     * @return
     */
    @PostMapping("/upload")
    public R upload(MultipartFile file) {
        //获取上传文件的文件名
        String oldFileName = file.getOriginalFilename();

        String realFileName = oldFileName.substring(0, oldFileName.lastIndexOf("."));

        String extName = oldFileName.substring(oldFileName.lastIndexOf("."));

        System.out.println("当前文件上传的路径为:   " + filePath);

        //拼装新文件路径及文件名
        String newPath = filePath + realFileName + "_" + System.currentTimeMillis() + extName;

        File newFile = new File(newPath);

        //当路径不存在的时候，进行路径创建和补充
        if (!newFile.exists()) {
            newFile.mkdirs();
        }

        //文件上传
        try {
            file.transferTo(newFile);
        } catch (IOException e) {
            e.printStackTrace();
            return R.error().setData(e.getMessage());
        }

        return R.ok();
    }


}
