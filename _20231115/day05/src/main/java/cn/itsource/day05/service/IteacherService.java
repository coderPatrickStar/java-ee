package cn.itsource.day05.service;

import cn.itsource.day05.entity.Teacher;

import java.math.BigInteger;
import java.util.List;


public interface IteacherService {
    List<Teacher> queryList();

    Teacher queryOne(BigInteger id);
    Integer saveOne(Teacher teacher);
    Integer updateOne(Teacher teacher);
    Integer delOne(BigInteger id);
}
