import com.mysql.cj.jdbc.MysqlDataSource;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Scanner;

public class Hello {
    public static void main(String[] args) throws SQLException {
        // testInsert();
        // testDelete();
        // testUpdate();
        testSelect();
    }

    public static void testSelect() throws SQLException {
        // 查找和 修改插入删除, 就有一些区别了!!
        // 插入删除修改, 执行完的返回结果, 只有一个 int 而已.
        // 查找操作, 执行完的结果, 是包含一组结果集合. (一张临时表), 此处就需要写一些额外的代码, 把这个临时表/结果集里的内容获取到!

        // 1. 创建数据源. 把数据库的位置信息, 设置进去
        DataSource dataSource = new MysqlDataSource();
        ((MysqlDataSource)dataSource).setURL("jdbc:mysql://127.0.0.1:3306/course?characterEncoding=utf8&useSSL=false");
        ((MysqlDataSource)dataSource).setUser("root");
        ((MysqlDataSource)dataSource).setPassword("666666");
        // 2. 建立连接
        Connection connection = dataSource.getConnection();
        // 3. 构造 SQL 语句
        String sql = "select * from student";
        PreparedStatement statement = connection.prepareStatement(sql);
        // 4. 执行 SQL 语句
        ResultSet resultSet = statement.executeQuery();
        // 5. 遍历结果集合
        while (resultSet.next()) {
            // resultSet 就表示当前的这一行
            // 从这一行里面就能获取到具体的列.
            int id = resultSet.getInt("id");
            String name = resultSet.getString("name");
            System.out.println("id: " + id + ", name: " + name);
        }
        // 6. 释放资源
        resultSet.close();
        statement.close();
        connection.close();
    }
}
