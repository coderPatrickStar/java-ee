package com.baidu.beans;

import com.baidu.model.UserInfo;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;

@Component
public class UserBeans {

    @Bean(name = {"userinfo","user"})
    public UserInfo user() {
        UserInfo userInfo = new UserInfo();
        userInfo.setId(1);
        userInfo.setName("Java");
        userInfo.setPassword("666");
        return userInfo;
    }
}
