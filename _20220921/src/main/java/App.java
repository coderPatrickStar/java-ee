import com.baidu.model.UserInfo;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class App {
    public static void main(String[] args) {
        ApplicationContext context = new ClassPathXmlApplicationContext("spring-config.xml");
        UserInfo userInfo  = context.getBean("user",UserInfo.class);
        UserInfo userInfo1 = context.getBean("userinfo",UserInfo.class);
        System.out.println(userInfo);
        System.out.println(userInfo1);
    }
}
