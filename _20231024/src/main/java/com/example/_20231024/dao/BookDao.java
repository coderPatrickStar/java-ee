package com.example._20231024.dao;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.example._20231024.domain.Book;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface BookDao extends BaseMapper<Book> {

}
