import org.ansj.domain.Term;
import org.ansj.splitWord.analysis.ToAnalysis;

import java.util.List;

public class TestAnsj {
    public static void main(String[] args) {
        String str = "i have a deam";
        List<Term> terms = ToAnalysis.parse(str).getTerms();
        for (Term term : terms){
            System.out.println(term);
        }
    }
}
