import java.io.File;
import java.lang.reflect.Array;
import java.lang.reflect.Field;
import java.util.ArrayList;

public class Parser {
    private static final String INPUT_PATH = "C:/Users/Administrator/Desktop/test/jdk-8u341-docs-all/docs/api";
    public void run(){
        //1.
        ArrayList<File> filelist = new ArrayList<>();
        enumFiles(INPUT_PATH,filelist);
        System.out.println(filelist);
        System.out.println(filelist.size());
        for (File f : filelist){
            parserHtml(f);
        }
    }

    private void parserHtml(File f) {
        String title = parserTitle(f);
        String url = parserUrl(f);
        String content = parserContent(f);
    }

    private String parserContent(File f) {
        return null;
    }

    private String parserUrl(File f) {
        String part1 = "https:/docs.oracle.com/javase/8/docs/api/";
        String part2 = f.getAbsolutePath().substring(INPUT_PATH.length());
        String result = part1 + part2;
        return result;
    }

    private String parserTitle(File f) {
        String name = f.getName();
        return name.substring(0,f.getName().length() - ".html".length());
    }

    private void enumFiles(String inputPath, ArrayList<File> filelist) {
        File rootPath = new File(inputPath);
        File[] files = rootPath.listFiles();

        for (File f : files){
            if(f.isDirectory()){
                enumFiles(f.getAbsolutePath(),filelist);
            }else {
                if(f.getAbsolutePath().endsWith(".html")){
                    filelist.add(f);
                }
            }
        }
    }

    public static void main(String[] args) {
        Parser parser = new Parser();
        parser.run();
    }
}
