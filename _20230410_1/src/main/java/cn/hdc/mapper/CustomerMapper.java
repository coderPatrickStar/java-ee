package cn.hdc.mapper;

import cn.hdc.pojo.Customer;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public interface CustomerMapper {
    public List<Customer> findCustomerByUsernameAndJobs(Customer customer);

    public List<Customer> findCustomerByUsernameOrJobs(Customer customer);

    public Integer updateCustomerBySet(Customer customer);

    public List<Customer> selectByArray(Integer[] ids);

    public List<Customer> selectByList(List<Integer> list);

    public List<Customer> selectByMap(Map<String, Object> map);
}
