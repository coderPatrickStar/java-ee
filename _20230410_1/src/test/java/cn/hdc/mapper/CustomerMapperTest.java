package cn.hdc.mapper;

import cn.hdc.pojo.Customer;
import cn.hdc.utils.MybatisUtils;
import org.apache.ibatis.session.SqlSession;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.util.*;

import static org.junit.Assert.*;

public class CustomerMapperTest {
    private SqlSession session;

    @Before
    public void setUp() throws Exception {
        session = MybatisUtils.getSession();
    }

    @After
    public void tearDown() throws Exception {
        session.close();
    }

    @Test
    public void findCustomerByUsernameAndJobs() {
        Customer customer = new Customer();
        //customer.setUsername("jack");
        //customer.setJobs("teacher");
        List<Customer> customers = session.selectList("cn.hdc.mapper.CustomerMapper.findCustomerByUsernameAndJobs", customer);
        for (Customer c : customers) {
            System.out.println(c);
        }
    }

    @Test
    public void findCustomerByUsernameOrJobs() {
        Customer customer = new Customer();
        // customer.setUsername("jack");
        customer.setJobs("teacher");
        List<Customer> customers = session.selectList("cn.hdc.mapper.CustomerMapper.findCustomerByUsernameOrJobs", customer);
        for (Customer c : customers) {
            System.out.println(c);
        }
    }

    @Test
    public void updateCustomerBySet() {
        Customer customer = new Customer();
        customer.setId(3);
        customer.setPhone("15132307449");
        int ret = session.update("cn.hdc.mapper.CustomerMapper.updateCustomerBySet", customer);
        if (ret > 0) {
            System.out.println("更新成功！");
        } else {
            System.out.println("更新失败！");
        }
        session.commit();
    }

    @Test
    public void selectByArray() {
        Integer[] ids = {1, 2, 3};
        List<Customer> customers = session.selectList("cn.hdc.mapper.CustomerMapper.selectByArray", ids);
        for (Customer customer : customers) {
            System.out.println(customer);
        }
    }

    @Test
    public void selectByList() {
        List<Integer> list = new ArrayList<>();
        list.add(1);
        list.add(2);
        list.add(3);
        List<Customer> customers = session.selectList("cn.hdc.mapper.CustomerMapper.selectByList", list);
        for (Customer customer : customers) {
            System.out.println(customer);
        }
    }

    @Test
    public void selectByMap() {
        Map<String, Object> map = new HashMap<>();
        List<Integer> ids = new ArrayList<>();
        ids.add(1);
        ids.add(2);
        ids.add(3);
        map.put("jobs", "teacher");
        map.put("id", ids);

        List<Customer> customers = session.selectList("cn.hdc.mapper.CustomerMapper.selectByMap",map);
        for (Customer customer : customers) {
            System.out.println(customer);
        }
    }
}