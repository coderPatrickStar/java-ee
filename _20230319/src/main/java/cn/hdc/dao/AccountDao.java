package cn.hdc.dao;

import cn.hdc.model.Account;

import java.util.List;

public interface AccountDao {
    public Integer addAccount(Account account);

    public Integer deleteAccount(Integer id);

    public Integer updateAccount(Account account);

    public Account findAccountById(Integer id);

    public List<Account> findAll();

    public void transfer(String outUser, String inUser, Double money);
}
