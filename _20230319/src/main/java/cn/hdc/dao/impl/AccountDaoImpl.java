package cn.hdc.dao.impl;

import cn.hdc.dao.AccountDao;
import cn.hdc.model.Account;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

public class AccountDaoImpl implements AccountDao {
    private JdbcTemplate jdbcTemplate;

    public JdbcTemplate getJdbcTemplate() {
        return jdbcTemplate;
    }

    public void setJdbcTemplate(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    @Override
    public Integer addAccount(Account account) {
        String sql = "insert into account(username,balance) values(?,?)";
        Object[] params = new Object[]{account.getUsername(), account.getBalance()};
        jdbcTemplate.update(sql, params);
        return jdbcTemplate.update(sql, params);
    }

    @Override
    public Integer deleteAccount(Integer id) {
        String sql = "delete from account where id = ?";
        return jdbcTemplate.update(sql, id);
    }

    @Override
    public Integer updateAccount(Account account) {
        String sql = "update account set username=?,balance=? where id=?";
        Object[] params = new Object[]{account.getUsername(), account.getBalance(), account.getId()};
        return jdbcTemplate.update(sql, params);
    }

    @Override
    public Account findAccountById(Integer id) {
        String sql = "select * from account where id = ?";
        RowMapper<Account> accountRowMapper = new BeanPropertyRowMapper<>(Account.class);
        Account account = jdbcTemplate.queryForObject(sql, accountRowMapper, id);
        return jdbcTemplate.queryForObject(sql, accountRowMapper, id);
    }

    @Override
    public List<Account> findAll() {
        String sql = "select * from account";
        RowMapper<Account> accountRowMapper = new BeanPropertyRowMapper<>(Account.class);
        List<Account> list = jdbcTemplate.query(sql, accountRowMapper);
        return list;
    }

    //模拟转账操作
    @Transactional(propagation = Propagation.REQUIRED,readOnly = false,isolation = Isolation.DEFAULT)
    @Override
    public void transfer(String outUser, String inUser, Double money) {
        //收款
        jdbcTemplate.update("update account set balance = balance + ? where username = ?", money, inUser);
        //模拟异常
        int i = 10 / 0;
        //付款
        jdbcTemplate.update("update account set balance = balance - ? where username = ?", money, outUser);
    }
}
