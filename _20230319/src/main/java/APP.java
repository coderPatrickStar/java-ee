import cn.hdc.dao.AccountDao;
import cn.hdc.dao.impl.AccountDaoImpl;
import cn.hdc.model.Account;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.jdbc.core.JdbcTemplate;

import java.util.List;

public class APP {
    public static void main(String[] args) {
        ApplicationContext context = new ClassPathXmlApplicationContext("applicationContext.xml");
        JdbcTemplate jdbcTemplate = (JdbcTemplate) context.getBean("jdbcTemplate");
        AccountDao accountDao = (AccountDao) context.getBean("accountDao");
        accountDao.transfer("zhangsan", "lisi", 100.0);

//        List<Account> list = accountDao.findAll();
//        list.forEach(account -> {
//            System.out.println(account);
//        });

//        Account account = accountDao.findAccountById(1);
//        System.out.println(account);

//        Integer ret = accountDao.deleteAccount(2);
//        if (ret > 0) {
//            System.out.println("删除成功！");
//        } else {
//            System.out.println("删除失败！");
//        }
//        Account account1 = new Account();
//        account1.setId(2);
//        account1.setUsername("zhangsan");
//        account1.setBalance(5000.02153);
//        Integer ret = accountDao.updateAccount(account1);
//        if (ret > 0) {
//            System.out.println("修改成功！");
//        } else {
//            System.out.println("修改失败！");
//        }

//        Account account = new Account();
//        account.setUsername("tom");
//        account.setBalance(1000.011);
//        Integer ret = accountDao.addAccount(account);
//        if (ret > 0) {
//            System.out.println("插入成功！");
//        } else {
//            System.out.println("插入失败！");
//        }


//        jdbcTemplate.execute("create table account" +
//                "(" +
//                "    id int primary key auto_increment," +
//                "    username varchar(50)," +
//                "    balance double" +
//                ");");
//        System.out.println("account表创建成功！");


        //        UserDaoImpl userDao = (UserDaoImpl) context.getBean("userDao");
        //        System.out.println(userDao.getJdbcTemplate());
    }
}
