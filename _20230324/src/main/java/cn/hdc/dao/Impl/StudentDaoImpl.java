package cn.hdc.dao.Impl;

import cn.hdc.dao.StudentDao;
import cn.hdc.entity.Stu;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.transaction.annotation.Transactional;

public class StudentDaoImpl implements StudentDao {
    private JdbcTemplate jdbcTemplate;

    public void setJdbcTemplate(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    public JdbcTemplate getJdbcTemplate() {
        return jdbcTemplate;
    }

    @Override
    @Transactional
    public Integer addStudent(Stu stu) {
        String sql = "insert into stu values (?,?,?,?)";
        Object[] params = new Object[]{stu.getSid(), stu.getSname(), stu.getAge(), stu.getCourse()};
        return jdbcTemplate.update(sql, params);
    }

    @Override
    @Transactional
    public Integer deleteStudent(Integer sid) {
        String sql = "delete from stu where sid = ?";
        int i = 10 / 0;
        return jdbcTemplate.update(sql, sid);
    }
}
