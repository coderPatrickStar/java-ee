package cn.hdc.dao;

import cn.hdc.entity.Stu;

public interface StudentDao {
    public Integer addStudent(Stu stu);

    public Integer deleteStudent(Integer sid);
}
