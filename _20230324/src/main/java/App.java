import cn.hdc.dao.StudentDao;
import cn.hdc.entity.Stu;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class App {
    public static void main(String[] args) {
        ApplicationContext context = new ClassPathXmlApplicationContext("application.xml");
        StudentDao studentDao = (StudentDao) context.getBean("studentDao");
        Stu stu = new Stu();
        stu.setSid(1);
        stu.setSname("ZhangSan");
        stu.setAge("21");
        stu.setCourse("Java");
        int ret = 0;

        ret = studentDao.addStudent(stu);
        if (ret > 0) {
            System.out.println("插入成功！");
        } else {
            System.out.println("插入失败！");
        }

        ret = studentDao.deleteStudent(1);
        if (ret > 0) {
            System.out.println("删除成功！");
        } else {
            System.out.println("删除失败！");
        }
    }
}
