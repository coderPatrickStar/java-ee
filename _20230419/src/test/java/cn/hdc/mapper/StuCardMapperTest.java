package cn.hdc.mapper;

import cn.hdc.pojo.StuCard;
import cn.hdc.utils.MybaitsUtils;
import org.apache.ibatis.session.SqlSession;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class StuCardMapperTest {
    private SqlSession session;

    @Before
    public void setUp() throws Exception {
        this.session = MybaitsUtils.getSession();
    }

    @After
    public void tearDown() throws Exception {
        this.session.commit();
        this.session.close();
    }


    @Test
    public void findCardById() {
        StuCard stuCard = session.selectOne("cn.hdc.mapper.StuCardMapper.findCardById", 1);
        System.out.println(stuCard);
    }
}