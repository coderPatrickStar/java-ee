package cn.hdc.mapper;

import cn.hdc.pojo.Person;
import cn.hdc.utils.MybaitsUtils;
import org.apache.ibatis.session.SqlSession;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class PersonMapperTest {

    private SqlSession session;

    @Before
    public void setUp() throws Exception {
        this.session = MybaitsUtils.getSession();
    }

    @After
    public void tearDown() throws Exception {
        this.session.commit();
        this.session.close();
    }

    @Test
    public void findPersonById() {
        Person person = session.selectOne("cn.hdc.mapper.PersonMapper.findPersonById", 1);
        System.out.println(person.getName());
        System.out.println("需要使用另外一张表的数据");
        System.out.println(person.getCard());
        System.out.println(person);
    }

    @Test
    public void findPersonById2() {
        Person person = session.selectOne("cn.hdc.mapper.PersonMapper.findPersonById2", 1);
        System.out.println(person);
    }

}