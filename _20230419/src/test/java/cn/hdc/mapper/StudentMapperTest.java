package cn.hdc.mapper;

import cn.hdc.pojo.Student;
import cn.hdc.utils.MybaitsUtils;
import org.apache.ibatis.session.SqlSession;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class StudentMapperTest {
    private SqlSession session;

    @Before
    public void setUp() throws Exception {
        this.session = MybaitsUtils.getSession();
    }

    @After
    public void tearDown() throws Exception {
        this.session.commit();
        this.session.close();
    }

    @Test
    public void findStudentById() {
        Student student = session.selectOne("cn.hdc.mapper.StudentMapper.findStudentById", 1);
        System.out.println(student);
    }

    @Test
    public void findStudentById2() {
        Student student = session.selectOne("cn.hdc.mapper.StudentMapper.findStudentById2", 1);
        System.out.println(student);
    }
}