package cn.hdc.mapper;

import cn.hdc.pojo.Book;
import cn.hdc.utils.MybaitsUtils;
import org.apache.ibatis.session.SqlSession;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class BookMapperTest {

    private SqlSession session;

    @Before
    public void setUp() throws Exception {
        this.session = MybaitsUtils.getSession();
    }

    @After
    public void tearDown() throws Exception {
        this.session.commit();
        this.session.close();
    }

    @Test
    public void findBookByIdTest1() {
        System.out.println("第一次查询");
        Book book = session.selectOne("cn.hdc.mapper.BookMapper.findBookById", 1);
        System.out.println(book);
        System.out.println("第二次查询");
        Book book2 = session.selectOne("cn.hdc.mapper.BookMapper.findBookById", 1);
        System.out.println(book2);
    }

    @Test
    public void findBookByIdTest2() {
        System.out.println("第一次查询");
        Book book = session.selectOne("cn.hdc.mapper.BookMapper.findBookById", 1);
        System.out.println(book);

        //更新表
        Book updatebook = new Book();
        updatebook.setId(2);
        updatebook.setBookName("三国演义");
        updatebook.setPrice(30);
        int ret = session.update("cn.hdc.mapper.BookMapper.updateBook", updatebook);
        if (ret > 0) {
            System.out.println("更新成功！");
        } else {
            System.out.println("更新失败！");
        }

        System.out.println("第二次查询");
        Book book2 = session.selectOne("cn.hdc.mapper.BookMapper.findBookById", 1);
        System.out.println(book2);
    }

    @Test
    public void findBookByIdTest3() {
        SqlSession session1 = MybaitsUtils.getSession();
        SqlSession session2 = MybaitsUtils.getSession();

        System.out.println("第一次查询");
        Book book = session1.selectOne("cn.hdc.mapper.BookMapper.findBookById", 1);
        System.out.println(book);
        session1.close();

        System.out.println("第二次查询");
        Book book2 = session2.selectOne("cn.hdc.mapper.BookMapper.findBookById", 1);
        System.out.println(book2);
        session2.close();
    }

    @Test
    public void findBookByIdTest4() {
        SqlSession session1 = MybaitsUtils.getSession();
        SqlSession session2 = MybaitsUtils.getSession();
        SqlSession session3 = MybaitsUtils.getSession();

        System.out.println("第一次查询");
        Book book = session1.selectOne("cn.hdc.mapper.BookMapper.findBookById", 1);
        System.out.println(book);
        session1.close();

        Book updatebook = new Book();
        updatebook.setId(2);
        updatebook.setBookName("三国演义");
        updatebook.setPrice(30);
        session3.update("cn.hdc.mapper.BookMapper.updateBook", updatebook);
        session3.commit();
        session3.close();

        System.out.println("第二次查询");
        Book book2 = session2.selectOne("cn.hdc.mapper.BookMapper.findBookById", 1);
        System.out.println(book2);
        session2.close();
    }
}