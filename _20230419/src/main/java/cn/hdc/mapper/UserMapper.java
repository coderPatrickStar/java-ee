package cn.hdc.mapper;

import cn.hdc.pojo.Orders;
import cn.hdc.pojo.User;

import java.util.List;

public interface UserMapper {
    public User findUserWithOrdersById(Integer id);

    public User findUserWithOrdersById2(Integer id);
}
