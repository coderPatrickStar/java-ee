package cn.hdc.mapper;

import cn.hdc.pojo.Orders;

import java.util.List;

public interface OrdersMapper {
    public Orders findOrdersById(Integer id);

    public Orders findOrdersById2(Integer id);

    public Orders findOrdersById3(Integer id);
}
