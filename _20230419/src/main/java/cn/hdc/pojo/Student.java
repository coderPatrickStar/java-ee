package cn.hdc.pojo;

public class Student {
    private int sid;
    private String sname;
    private int age;
    private String course;

    private int cardid;
    private StuCard card;

    @Override
    public String toString() {
        return "Student{" +
                "sid=" + sid +
                ", sname='" + sname + '\'' +
                ", age=" + age +
                ", course='" + course + '\'' +
                ", cardid=" + cardid +
                ", card=" + card +
                '}';
    }

    public int getSid() {
        return sid;
    }

    public void setSid(int sid) {
        this.sid = sid;
    }

    public String getSname() {
        return sname;
    }

    public void setSname(String sname) {
        this.sname = sname;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getCourse() {
        return course;
    }

    public void setCourse(String course) {
        this.course = course;
    }

    public int getCardid() {
        return cardid;
    }

    public void setCardid(int cardid) {
        this.cardid = cardid;
    }

    public StuCard getCard() {
        return card;
    }

    public void setCard(StuCard card) {
        this.card = card;
    }
}
