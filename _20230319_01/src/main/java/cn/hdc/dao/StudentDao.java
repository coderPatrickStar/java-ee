package cn.hdc.dao;

import cn.hdc.entity.Student;

public interface StudentDao {
    public Student findStudentByUsernameAndPassword(String username, String password);
}
