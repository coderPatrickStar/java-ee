package cn.hdc.controller;

import cn.hdc.dao.StudentDao;
import cn.hdc.entity.Student;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.util.Scanner;

public class StudentController {
    public static void main(String[] args) {
        ApplicationContext context = new ClassPathXmlApplicationContext("applicationContext.xml");
        System.out.println("欢迎来到学生管理系统！");
        System.out.println("请输入用户名：");
        Scanner scanner = new Scanner(System.in);
        String username = scanner.nextLine();

        System.out.println("请输入" + username + "的密码：");
        String password = scanner.nextLine();

        StudentDao studentDao = (StudentDao) context.getBean("studentDao");
        Student student = studentDao.findStudentByUsernameAndPassword(username, password);
        if (student == null) {
            System.out.println("登录失败,用户名或密码错误");
        } else {
            System.out.println("登录成功！");
            System.out.println(username + "是" + student.getCourse() + "班的");
        }
    }
}
