package com.example._20230103.config;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.HashMap;

@ControllerAdvice
@ResponseBody
public class ExcepetionAdvice {
//    @ExceptionHandler(ArithmeticException.class)
//    public HashMap<String, Object> MyArithmetionException(ArithmeticException e) {
//        HashMap<String, Object> map = new HashMap<>();
//        map.put("succ", "-1");
//        map.put("msg", "程序发生了异常：" + e.getMessage());
//        map.put("data", "");
//        return map;
//    }
//
//    @ExceptionHandler(NumberFormatException.class)
//    public HashMap<String, Object> MyNullPointerException(NumberFormatException e) {
//        HashMap<String, Object> map = new HashMap<>();
//        map.put("succ", "-1");
//        map.put("msg", "程序异常：" + e.getMessage());
//        map.put("data", "");
//        return map;
//    }

    @ExceptionHandler(Exception.class)
    public HashMap<String, Object> MyNullPointerException(Exception e) {
        System.out.println("-------------------默认异常-------------------");
        HashMap<String, Object> map = new HashMap<>();
        map.put("succ", "-1");
        map.put("msg", "程序异常：" + e.getMessage());
        map.put("data", "");
        return map;
    }
}
