//package cn.hdc.entity;
//
//
//import org.springframework.cglib.proxy.Enhancer;
//import org.springframework.cglib.proxy.MethodInterceptor;
//import org.springframework.cglib.proxy.MethodProxy;
//
//import java.lang.reflect.Method;
//
//public class MyCglibProxy implements MethodInterceptor {
//    public Object createProxy(Object object) {
//        Enhancer enhancer = new Enhancer();
//        enhancer.setSuperclass(object.getClass());
//        enhancer.setCallback(this);
//        return enhancer.create();
//    }
//
//    /**
//     * 增强功能的具体实现
//     *
//     * @param o           代理对象本身
//     * @param method      被代理的方法
//     * @param objects     方法需要的参数
//     * @param methodProxy 代理后的方法
//     * @return
//     * @throws Throwable
//     */
//    @Override
//    public Object intercept(Object o, Method method, Object[] objects, MethodProxy methodProxy) throws Throwable {
//        MyAspect myAspect = new MyAspect();
//
//        myAspect.check_permission();
//        //保证原有功能的执行
//        Object res = methodProxy.invokeSuper(o, objects);
//        myAspect.log();
//        return res;
//    }
//}
