package cn.hdc.demo3.entity;


import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;

//切面
public class XmlAdvice {
    /**
     * @param joinpoint 内部封装了切入点的相关信息
     */
    public void before(JoinPoint joinpoint) {
        System.out.println("前置通知");
        System.out.println("目标类：" + joinpoint.getTarget());
        System.out.println("切入点的方法名：" + joinpoint.getSignature().getName());
    }

    public void after(JoinPoint joinPoint) {
        System.out.println("后置通知");
    }

    public void around(ProceedingJoinPoint pjp) throws Throwable {
        System.out.println("自定义环绕通知111，在切入点之前执行");
        //执行原有功能
        Object res = pjp.proceed();
        System.out.println("自定义环绕通知222，在切入点之后执行");
        //返回原有方法的返回值
    }

    public void after_returning() {
        System.out.println("返回通知，返回值结束才会执行（如果出现异常，不会执行）");
    }

    public void after_throwing(){
        System.out.println("异常通知，出现异常才会通知");
    }
}
