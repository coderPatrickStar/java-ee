package cn.hdc.demo1.dao;

public interface UserDao {
    public void addUser();
    public void deleteUser();
}
