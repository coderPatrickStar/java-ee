package cn.hdc.factory;

import cn.hdc.beans.Bean2;
import cn.hdc.beans.Bean3;
import cn.hdc.beans.Bean5;
import cn.hdc.beans.Bean6;

public class MyBeanFactory {
    public Bean6 createBean() {
        System.out.println("实例工厂实例化");
        return new Bean6();
    }

//    public static Bean5 createBean() {
//        System.out.println("静态工厂实例化");
//        return new Bean5();
//    }

//    public Bean3 createBean() {
//        System.out.println("实例工厂实例化");
//        return new Bean3();
//    }

//    public static Bean2 createBean() {
//        System.out.println("静态工厂实例化");
//        return new Bean2();
//    }
}
