package cn.hdc.test;


class ListNode {
    int val;
    ListNode next;

    ListNode() {
    }

    ListNode(int val) {
        this.val = val;
    }

    ListNode(int val, ListNode next) {
        this.val = val;
        this.next = next;
    }
}

class Solution {
    public ListNode addTwoNumbers(ListNode l1, ListNode l2) {
        ListNode head = new ListNode();
        ListNode cur = l1;
        int n = 0;
        int m = 0;
        int cnt = 0;
        while (cur != null) {
            n += cur.val * (int) Math.pow(10, cnt);
            cnt++;
            cur = cur.next;
        }

        cnt = 0;
        cur = l2;
        while (cur != null) {
            m += cur.val * (int) Math.pow(10, cnt);
            cnt++;
            cur = cur.next;
        }
        int count = n + m;
        cur = head;
        while (count > 0) {
            ListNode node = new ListNode(count % 10);
            cur.next = node;
            cur = cur.next;
            count /= 10;
        }
        if ((l1.val == l2.val) && (l1.val == 0)) {
            ListNode node1 = new ListNode(0);
            ListNode node2 = new ListNode(0);
            head.next = node1;
            return head.next;
        }
        return head.next;
    }
}

public class Test3 {
    public static void main(String[] args) {
        Solution solution1 = new Solution();
        ListNode l1 = new ListNode(2);
        l1.next = new ListNode(4);
        l1.next.next = new ListNode(3);

        ListNode l2 = new ListNode(5);
        l2.next = new ListNode(6);
        l2.next.next = new ListNode(4);
        ListNode cur = solution1.addTwoNumbers(l1, l2);
        System.out.println(cur.val);
        System.out.println(cur.next.val);
        System.out.println(cur.next.next.val);
    }
}
