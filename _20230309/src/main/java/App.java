//import cn.hdc.entity.MyCglibProxy;
//import cn.hdc.factory.MyBeanFactory;


import cn.hdc.demo2.entity.CglibProxy;
//import cn.hdc.demo3.dao.UserDao;
import cn.hdc.demo4.dao.UserDao;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class App {
    public static void main(String[] args) {
        ApplicationContext context = new ClassPathXmlApplicationContext("application.xml");
        UserDao userDao = (UserDao) context.getBean("userDao");
        userDao.insert();
        userDao.select();
        userDao.update();
        userDao.delete();

//        CglibProxy cglibProxy = new CglibProxy();//创建代理对象
//        UserDao userDao = new UserDao();//创建目标对象
//        //获取增强后的目标对象
//        UserDao userDao1 = (UserDao) cglibProxy.createProxy(userDao);
//        userDao1.addUser();
//        userDao1.deleteUser();


//        MyProxy jdkProxy = new MyProxy();
//        UserDao userDao = new UserDaoImpl();
//        UserDao userDao1 = (UserDao) jdkProxy.createProxy(userDao);
//        userDao1.addUser();
//        userDao1.deleteUser();


//        MyCglibProxy cglibProxy = new MyCglibProxy();
//        UserDao userDao = new UserDao();
//        UserDao userDao1 = (UserDao) cglibProxy.createProxy(userDao);
//        userDao1.saveUser();
//        userDao1.deleteUser();


//        ApplicationContext context = new ClassPathXmlApplicationContext("application.xml");
//        Bean2 bean2 = (Bean2) context.getBean("bean2");
//        Bean3 bean3 = (Bean3) context.getBean("bean3");
//        Bean4 bean4 = (Bean4) context.getBean("bean4");
//        Bean5 bean5 = MyBeanFactory.createBean();
//        Bean6 bean6 = (Bean6) new MyBeanFactory().createBean();
    }
}
