package cn.hdc.demo4.dao;

public interface UserDao {
    public void insert();

    public void delete();

    public void update();

    public void select();
}
