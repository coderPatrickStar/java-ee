package cn.hdc.dao;

import cn.hdc.entity.Student;

import java.util.List;

public interface StudentDao {
    //增
    public Integer addStudent(Student student);

    //删
    public Integer deleteStudent(Integer id);

    //改
    public Integer updateStudent(Student student);

    //查
    public List<Student> selectAll();

    //根据Id查
    public Student selectById(Integer id);
}
