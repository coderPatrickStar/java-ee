package cn.hdc.dao.AccountDaoImpl;

import cn.hdc.dao.StudentDao;
import cn.hdc.entity.Student;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;

import java.util.Collections;
import java.util.List;

public class StudentDaoImpl implements StudentDao {
    private JdbcTemplate jdbcTemplate;

    public JdbcTemplate getJdbcTemplate() {
        return jdbcTemplate;
    }

    public void setJdbcTemplate(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    //宗晓卓
    @Override
    public Integer addStudent(Student student) {
        String sql = "insert into student values (?,?,?,?)";
        Object[] params = new Object[]{student.getId(), student.getName(), student.getSex(), student.getBorn()};
        return jdbcTemplate.update(sql, params);
    }

    //谭磊
    @Override
    public Integer deleteStudent(Integer id) {
        String sql = "delete from student where id = ?";
        return jdbcTemplate.update(sql, id);
    }

    //刘德利
    @Override
    public Integer updateStudent(Student student) {
        String sql = "update student set id = ?, name = ?,sex = ?,born = ?";
        Object[] params = new Object[]{student.getId(), student.getName(), student.getSex(), student.getBorn()};
        return jdbcTemplate.update(sql, params);
    }

    //张文凯
    @Override
    public List<Student> selectAll() {
        String sql = "select * from student";
        RowMapper<Student> rowMapper = new BeanPropertyRowMapper<>(Student.class);
        List<Student> list = jdbcTemplate.query(sql, rowMapper);
        return list;
    }

    //刘启
    @Override
    public Student selectById(Integer id) {
        String sql = "select * from student where id = ?";
        RowMapper<Student> rowMapper = new BeanPropertyRowMapper<>(Student.class);
        Student student = jdbcTemplate.queryForObject(sql, rowMapper, id);
        return student;
    }
}
