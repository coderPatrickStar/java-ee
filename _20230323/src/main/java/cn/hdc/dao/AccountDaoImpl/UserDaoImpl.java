package cn.hdc.dao.AccountDaoImpl;
 
import org.springframework.jdbc.core.JdbcTemplate;
 
public class UserDaoImpl {
    private JdbcTemplate jdbcTemplate;
 
    public void setJdbcTemplate(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }
 
    public JdbcTemplate getJdbcTemplate() {
        return jdbcTemplate;
    }
}