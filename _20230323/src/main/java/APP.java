import cn.hdc.dao.AccountDaoImpl.UserDaoImpl;
import cn.hdc.dao.StudentDao;
import cn.hdc.entity.Student;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.jdbc.core.JdbcTemplate;

import java.sql.Date;
import java.util.List;

public class APP {
    public static void main(String[] args) {
        ApplicationContext context = new ClassPathXmlApplicationContext("application.xml");
        JdbcTemplate jdbcTemplate = (JdbcTemplate) context.getBean("jdbcTemplate");
        StudentDao studentDao = (StudentDao) context.getBean("studentDao");
//        Student student = new Student();
//        //增加  //宗晓卓
//        student.setId(3);
//        student.setName("giao");
//        student.setSex("男");
//        student.setBorn(Date.valueOf("2023-03-23"));
//
//        Integer ret  = studentDao.addStudent(student);
//        if(ret > 0){
//            System.out.println("插入成功！");
//        }else {
//            System.out.println("插入失败");
//        }

        //查找全部 //张文凯
//        List<Student> student = studentDao.selectAll();
//        System.out.println(student);
//
//        //根据id查  //刘启
        Student student1 = studentDao.selectById(1);
        System.out.println(student1);

        //删除  //谭磊
//        Integer ret = studentDao.deleteStudent(2);
//        if (ret > 0) {
//            System.out.println("删除成功！");
//        } else {
//            System.out.println("删除失败！");
//        }

//        //修改 //刘德利
//        Student student = new Student();
//        student.setId(3);
//        student.setName("lisi");
//        student.setSex("女");
//        student.setBorn(Date.valueOf("2023-01-02"));
//        Integer ret = studentDao.updateStudent(student);
//        if (ret > 0) {
//            System.out.println("修改成功！");
//        } else {
//            System.out.println("修改失败！");
//        }


    }
}