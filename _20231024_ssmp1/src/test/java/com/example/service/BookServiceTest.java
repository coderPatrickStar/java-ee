package com.example.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.example.domain.Book;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
class BookServiceTest {
    @Autowired
    private BookService bookService;

    @Test
    void save() {
        Book book = new Book();
        book.setType("测试数据123");
        book.setName("测试数据123");
        book.setDescription("测试数据123");

        Boolean ret = bookService.save(book);

        if (ret == false) {
            System.out.println("新增失败!");
        } else {
            System.out.println("新增成功!");
        }
    }

    @Test
    void update() {
        Book book = new Book();
        book.setId(13);
        book.setType("aaaa");
        book.setName("aaaa");
        book.setDescription("aaaa");

        Boolean ret = bookService.update(book);

        if (ret == false) {
            System.out.println("修改失败!");
        } else {
            System.out.println("修改成功!");
        }
    }

    @Test
    void delete() {
        Boolean ret = bookService.delete(16);

        if (ret == false) {
            System.out.println("删除失败!");
        } else {
            System.out.println("删除成功!");
        }
    }

    @Test
    void getById() {
        System.out.println(bookService.getById(4));
    }

    @Test
    void getAll() {
        System.out.println(bookService.getAll());
    }

    @Test
    void getPage() {
        IPage page = bookService.getPage(2, 5);
        System.out.println(page.getCurrent());
        System.out.println(page.getSize());
        System.out.println(page.getTotal());
        System.out.println(page.getPages());
        System.out.println(page.getRecords());
    }
}