package com.example.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.example.domain.Book;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
class IBookServiceTest {
    @Autowired
    private IBookService bookService;


    @Test
    void save() {
        Book book = new Book();
        book.setType("测试数据123");
        book.setName("测试数据123");
        book.setDescription("测试数据123");

        Boolean ret = bookService.save(book);

        if (ret == false) {
            System.out.println("新增失败!");
        } else {
            System.out.println("新增成功!");
        }
    }

    @Test
    void update() {
        Book book = new Book();
        book.setId(18);
        book.setType("aaaa");
        book.setName("aaaa");
        book.setDescription("aaaa");

        Boolean ret = bookService.updateById(book);

        if (ret == false) {
            System.out.println("修改失败!");
        } else {
            System.out.println("修改成功!");
        }
    }

    @Test
    void delete1() {
        Boolean ret = bookService.removeById(17);

        if (ret == false) {
            System.out.println("删除失败!");
        } else {
            System.out.println("删除成功!");
        }
    }

    @Test
    void getById() {
        System.out.println(bookService.getById(4));
    }

    @Test
    void getAll() {
        System.out.println(bookService.list());
    }

    @Test
    void getPage() {
        IPage page = new Page<Book>(2, 5);
        bookService.page(page);
        System.out.println(page.getCurrent());
        System.out.println(page.getSize());
        System.out.println(page.getTotal());
        System.out.println(page.getPages());
        System.out.println(page.getRecords());
    }

    //    ------------------------追加的功能-----------------------------------
    @Test
    void insert() {
        Book book = new Book();
        book.setType("测试数据123");
        book.setName("测试数据123");
        book.setDescription("测试数据123");

        Boolean ret = bookService.insert(book);

        if (ret == false) {
            System.out.println("新增失败!");
        } else {
            System.out.println("新增成功!");
        }
    }

    @Test
    void modify() {
        Book book = new Book();
        book.setId(18);
        book.setType("aaaa");
        book.setName("aaaa");
        book.setDescription("aaaa");

        Boolean ret = bookService.modify(book);

        if (ret == false) {
            System.out.println("修改失败!");
        } else {
            System.out.println("修改成功!");
        }
    }

    @Test
    void delete() {
        Boolean ret = bookService.delete(17);

        if (ret == false) {
            System.out.println("删除失败!");
        } else {
            System.out.println("删除成功!");
        }
    }

    @Test
    void get() {
        System.out.println(bookService.get(4));
    }
}