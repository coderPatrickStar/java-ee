package com.example.dao;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.example.domain.Book;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;


@SpringBootTest
class BookDaoTest {
    @Autowired
    public BookDao bookDao;

    @Test
    void testGetById() {
        System.out.println(bookDao.selectById(1));
    }

    @Test
    void testSave() {
        Book book = new Book();
        book.setType("测试数据123");
        book.setName("测试数据123");
        book.setDescription("测试数据123");

        int ret = bookDao.insert(book);

        if (ret == 0) {
            System.out.println("新增失败!");
        } else {
            System.out.println("新增成功!");
        }
    }

    @Test
    void testUpdate() {
        Book book = new Book();
        book.setId(13);
        book.setType("aaaa");
        book.setName("aaaa");
        book.setDescription("aaaa");

        int ret = bookDao.updateById(book);

        if (ret == 0) {
            System.out.println("修改失败!");
        } else {
            System.out.println("修改成功!");
        }
    }

    @Test
    void testDelete() {
        int ret = bookDao.deleteById(13);

        if (ret == 0) {
            System.out.println("删除失败!");
        } else {
            System.out.println("删除成功!");
        }
    }

    @Test
    void testGetAll() {
        System.out.println(bookDao.selectList(null));
    }

    @Test
    void testGetPage() {
        IPage page = new Page(2, 5);
        bookDao.selectPage(page, null);
        System.out.println(page.getCurrent());
        System.out.println(page.getSize());
        System.out.println(page.getTotal());
        System.out.println(page.getPages());
        System.out.println(page.getRecords());
    }

    @Test
    void testGetBy() {
        QueryWrapper<Book> qw = new QueryWrapper<>();
        qw.like("name", "Spring");
        bookDao.selectList(qw);
    }

    @Test
    void testGetBy2() {
        String name = "Spring";
        LambdaQueryWrapper<Book> lqw = new LambdaQueryWrapper<>();
        lqw.like(name != null, Book::getName, name);
        bookDao.selectList(lqw);
    }
}