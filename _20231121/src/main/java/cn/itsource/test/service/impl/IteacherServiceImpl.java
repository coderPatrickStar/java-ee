package cn.itsource.test.service.impl;

import cn.itsource.test.entity.Teacher;
import cn.itsource.test.mapper.TeacherMapper;
import cn.itsource.test.service.IteacherService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigInteger;
import java.util.List;

@Service
public class IteacherServiceImpl implements IteacherService {
    @Autowired
    private TeacherMapper mapper;
    /**
     * 查询所有的数据
     * @return 返回数据集合
     */
    /**
     * 查询所有的数据
     * @return 返回数据集合
     */
    @Override
    public List<Teacher> queryList() {
        return mapper.queryList();
    }

    /**
     * 根据Id 获取一条数据
     * @param id 查询数据的Id
     * @return  返回数据信息
     */

    @Override
    public Teacher queryOne(BigInteger id) {
        return mapper.queryOne(id);
    }

    /**
     * 新增一条数据
     * @param teacher 数据的信息
     * @return 是否成功
     */

    @Override
    public Integer saveOne(Teacher teacher) {
        return mapper.saveOne(teacher);
    }
    /**
     * 修改一条数据
     * @param teacher 数据的信息
     * @return 是否成功
     */

    @Override
    public Integer updateOne(Teacher teacher) {
        return mapper.updateOne(teacher);
    }
    /**
     * 删除一条数据
     * @param id 数据id
     * @return 是否成功
     */

    @Override
    public Integer delOne(BigInteger id) {
        return mapper.delOne(id);
    }

}
