package cn.itsource.test.service;


import cn.itsource.test.entity.Teacher;

import java.math.BigInteger;
import java.util.List;


public interface IteacherService {
    List<Teacher> queryList();

    Teacher queryOne(BigInteger id);
    Integer saveOne(Teacher teacher);
    Integer updateOne(Teacher teacher);
    Integer delOne(BigInteger id);
}
