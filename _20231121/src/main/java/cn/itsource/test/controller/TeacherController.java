package cn.itsource.test.controller;

import cn.itsource.test.common.R;
import cn.itsource.test.entity.Teacher;
import cn.itsource.test.service.IteacherService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.math.BigInteger;


@RestController
@RequestMapping("/teacher")
public class TeacherController {

    @Autowired
    private IteacherService service;

    /**
     * 查询所有数据
     *
     * @return 返回数据列表
     */
    @GetMapping("/list")
    public R queryList() {
        return R.ok().setData(service.queryList());
    }

    /**
     * 根据ID 查询一个数据
     *
     * @param id 数据ID
     * @return 当前数据信息
     */
    @GetMapping("/getOne/{id}")
    public R queryOne(@PathVariable BigInteger id) {
        return R.ok().setData(service.queryOne(id));
    }

    /**
     * 保存一个数据
     *
     * @param teacher 数据信息
     * @return 返回是否成功
     */
    @PostMapping("/saveOne")
    public R saveOne(Teacher teacher) {
        service.saveOne(teacher);
        return R.ok();
    }

    /**
     * 修改一个数据
     *
     * @param teacher 数据信息
     * @return 返回是否成功
     */
    @PutMapping("/editOne")
    public R updateOne(Teacher teacher) {
        service.updateOne(teacher);
        return R.ok();
    }

    /**
     * 删除一个数据
     *
     * @param id 数据ID
     * @return 返回是否成功
     */
    @DeleteMapping("/delOne")
    public R delOne(BigInteger id) {
        service.delOne(id);
        return R.ok();
    }
}
