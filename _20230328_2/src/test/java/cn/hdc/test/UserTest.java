package cn.hdc.test;

import cn.hdc.pojo.User;
import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import org.junit.Test;

import java.io.IOException;
import java.io.InputStream;

public class UserTest {
    @Test
    public void findById() throws IOException {
        //1.获取核心配置文件
        InputStream inputStream = Resources.getResourceAsStream("mybatis-config.xml");
        //2.创建sqlSessionFactory工厂对象
        SqlSessionFactory sqlSessionFactory = new SqlSessionFactoryBuilder().build(inputStream);
        //3.创建sqlSession对象
        SqlSession session = sqlSessionFactory.openSession();
        //4.执行sql语句,sql语句的唯一标识：namespace.statementId
        User user = session.selectOne("cn.hdc.pojo.User.findById", 1);
        System.out.println(user);
        //5.释放资源
        session.close();
    }
}
