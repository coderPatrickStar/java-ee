package cn.hdc.mapper;

import cn.hdc.pojo.Person;

import java.util.List;

public interface PersonMapper {
    public Person findPersonById(Integer id);

    public Person findPersonById2(Integer id);
}
