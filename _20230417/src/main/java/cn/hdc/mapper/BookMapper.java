package cn.hdc.mapper;

import cn.hdc.pojo.Book;

public interface BookMapper {
    public Book findBookById(Integer id);

    public Integer updateBook(Book book);
}
