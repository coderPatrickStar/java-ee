package cn.hdc.mapper;

import cn.hdc.pojo.StuCard;

public interface StuCardMapper {
    public StuCard findCardById(Integer id);
}
