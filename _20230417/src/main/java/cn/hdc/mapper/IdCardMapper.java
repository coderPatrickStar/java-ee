package cn.hdc.mapper;

import cn.hdc.pojo.IdCard;

import java.util.List;

public interface IdCardMapper {
    public IdCard findCodeById(Integer id);
}
