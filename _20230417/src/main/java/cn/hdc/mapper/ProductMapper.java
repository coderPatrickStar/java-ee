package cn.hdc.mapper;

import cn.hdc.pojo.Product;

public interface ProductMapper {
    public Product findProductById(Integer id);
}
