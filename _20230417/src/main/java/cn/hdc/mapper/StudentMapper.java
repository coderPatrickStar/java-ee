package cn.hdc.mapper;

import cn.hdc.pojo.Student;

public interface StudentMapper {
    public Student findStudentById(Integer id);
    public Student findStudentById2(Integer id);
}
