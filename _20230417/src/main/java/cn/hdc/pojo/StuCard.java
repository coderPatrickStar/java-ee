package cn.hdc.pojo;

public class StuCard {
    private int cid;
    private double balance;

    private Student student;

    @Override
    public String toString() {
        return "StuCard{" +
                "cid=" + cid +
                ", balance=" + balance +
                ", student=" + student +
                '}';
    }

    public int getCid() {
        return cid;
    }

    public void setCid(int cid) {
        this.cid = cid;
    }

    public double getBalance() {
        return balance;
    }

    public void setBalance(double balance) {
        this.balance = balance;
    }

    public Student getStudent() {
        return student;
    }

    public void setStudent(Student student) {
        this.student = student;
    }
}
