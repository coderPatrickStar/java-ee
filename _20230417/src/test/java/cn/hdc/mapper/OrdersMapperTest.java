package cn.hdc.mapper;

import cn.hdc.pojo.Orders;
import cn.hdc.utils.MybaitsUtils;
import org.apache.ibatis.session.SqlSession;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.util.List;

import static org.junit.Assert.*;

public class OrdersMapperTest {

    private SqlSession session;

    @Before
    public void setUp() throws Exception {
        this.session = MybaitsUtils.getSession();
    }

    @After
    public void tearDown() throws Exception {
        this.session.commit();
        this.session.close();
    }

    @Test
    public void findOrdersById() {
        List<Orders> list = session.selectList("cn.hdc.mapper.OrdersMapper.findOrdersById", 1);
        System.out.println(list);
    }

    @Test
    public void findOrdersById2() {
        Object order = session.selectOne("cn.hdc.mapper.OrdersMapper.findOrdersById2", 1);
        System.out.println(order);
    }

    @Test
    public void findOrdersById3() {
        Object order = session.selectOne("cn.hdc.mapper.OrdersMapper.findOrdersById3", 1);
        System.out.println(order);
    }
}