package cn.hdc.mapper;

import cn.hdc.pojo.User;
import cn.hdc.utils.MybaitsUtils;
import org.apache.ibatis.session.SqlSession;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class UserMapperTest {

    private SqlSession session;

    @Before
    public void setUp() throws Exception {
        this.session = MybaitsUtils.getSession();
    }

    @After
    public void tearDown() throws Exception {
        this.session.commit();
        this.session.close();
    }

    @Test
    public void findUserWithOrdersById() {
        User user = session.selectOne("cn.hdc.mapper.UserMapper.findUserWithOrdersById", 1);
        System.out.println(user);
    }

    @Test
    public void findUserWithOrdersById2() {
        User user = session.selectOne("cn.hdc.mapper.UserMapper.findUserWithOrdersById2", 1);
        System.out.println(user);
    }
}