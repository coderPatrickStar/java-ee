import com.sun.net.httpserver.HttpServer;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.swing.*;
import java.io.IOException;

@WebServlet("/hello")
public class TestInterface extends HttpServlet implements SayHello {
    public static void main(String[] args) {
        TestInterface testInterface = new TestInterface();
        testInterface.sayhello();
        System.out.println("heoooooo");
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.getWriter().write("hello");
    }

    @Override
    public void sayhello() {
        System.out.println("haha");
    }
}
