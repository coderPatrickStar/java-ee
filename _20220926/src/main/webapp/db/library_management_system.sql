create database library_management_system;

use library_management_system;

create table book(
                     bookId int primary key auto_increment,
                     name varchar(50),
                     author varchar(50),
                     price int,
                     type varchar(50),
                     isBorrowed int
);

create table user(
                     userId int primary key auto_increment,
                     name varchar(50) unique,
                     password varchar(50),
                     isAdmin int
);

insert into book values (null,'西游记','吴承恩',10000,'古典名著',0);
insert into book values (null,'三国演义','罗贯中',12000,'古典名著',0);
insert into book values (null,'红楼梦','曹雪芹',9000,'古典名著',0);

insert into user values (null,'admin','123',1);
insert into user values (null,'tanlei','123',0);

use course;

create table student(
                        id int primary key auto_increment,
                        name varchar(20)
);

insert into student values (null,'tanlei'),(null,'lisi');