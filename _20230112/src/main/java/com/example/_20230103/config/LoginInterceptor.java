package com.example._20230103.config;

import com.example._20230103.commons.ApplicationVariable;
import com.fasterxml.jackson.databind.cfg.HandlerInstantiator;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.web.servlet.server.Session;
import org.springframework.web.servlet.HandlerInterceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
@Slf4j
//用户登录的拦截器
public class LoginInterceptor implements HandlerInterceptor {
    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        //用户登录的验证，如果验证通过，那么返回true,否则返回false
        HttpSession session = request.getSession(false);
        if (session != null && session.getAttribute(ApplicationVariable.USER_SESSION_KEY) != null) {
            //已经登录
            log.info("-----------拦截器：登录验证通过---------------");
            return true;
        }
        log.info("-----------拦截器：登录验证未通过---------------");
        response.setStatus(401);//指定返回的状态码，为无权限
        return false;
    }
}
