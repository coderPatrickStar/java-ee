package com.example._20230103.service;

import com.example._20230103.mapper.SysLogMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

@Service
public class SysLogService {
    @Autowired
    private SysLogMapper sysLogMapper;

    @Transactional(propagation = Propagation.REQUIRED)
    public int add(String desc) {
        int result = sysLogMapper.addLog(desc);
        System.out.println("日志添加：" + result);
//        Object obj = null;
//        obj.hashCode();
        return result;
    }
}
