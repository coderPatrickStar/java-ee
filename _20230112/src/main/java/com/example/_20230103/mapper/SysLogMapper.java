package com.example._20230103.mapper;

import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface SysLogMapper {
    public int addLog(String desc);
}
