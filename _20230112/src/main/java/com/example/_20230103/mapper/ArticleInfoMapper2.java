package com.example._20230103.mapper;

import com.example._20230103.model.ArticleInfo2;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface ArticleInfoMapper2 {
    public ArticleInfo2 getArticleById(int id);
}
