package com.example._20230103.mapper;

import com.example._20230103.model.ArticleInfo;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface ArticleInfoMapper {


    public int updateArticle(Integer id, String title, String content, Integer rcount);

    public List<ArticleInfo> getListByTitleOrUid(String title, Integer uid);

    public ArticleInfo getArticeByAId(int id);
}
