package com.example._20230103.controller;

import com.example._20230103.commons.ApplicationVariable;
import com.example._20230103.model.User;
import com.example._20230103.model.UserInfo;
import com.example._20230103.service.SysLogService;
import com.example._20230103.service.UserService;
import com.sun.xml.internal.ws.api.model.CheckedException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.system.ApplicationHome;
import org.springframework.core.io.ClassPathResource;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.interceptor.TransactionAspectSupport;
import org.springframework.util.ClassUtils;
import org.springframework.util.ResourceUtils;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.*;
import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.UUID;

@Controller
@RequestMapping("/user")
@ResponseBody   //当前类中所有方法，将返回一个非静态页面的数据
public class UserController {
    @Autowired
    private UserService userService;

    @Autowired
    private SysLogService sysLogService;

    @RequestMapping("/adduser2")
    @Transactional(propagation = Propagation.REQUIRED)
    public String addUser2(String username, String password) {
        if (StringUtils.hasLength(username) && StringUtils.hasLength(password)) {
            userService.addUser(username, password);
            //sysLogService.add("添加用户：" + username);
            return "方法执行完成";
        }
        return "请先传递参数！";
    }

    @Transactional//(rollbackFor = Exception.class)
    @RequestMapping("/adduser")
    public String addUser(String username, String password) throws Exception {
        int result = userService.addUser(username, password);
        try {
            int count = 10 / 0;
        } catch (Exception e) {
            System.out.println(e.getMessage());
            throw new Exception();
        }
        return "受影响行数：" + result;
    }

    @RequestMapping("/login2")
    public boolean login2(String username, String password) {
        return true;
    }

    @RequestMapping("/login3")
    public HashMap<String, Object> login3(String username, String password) {
        HashMap<String, Object> map = new HashMap<>();
        map.put("succ", 200);
        map.put("data", true);
        map.put("msg", "");
        return map;
    }

    @RequestMapping("/login")
    public String login(HttpServletRequest request, String username, String password) {
        if (StringUtils.hasLength(username) && StringUtils.hasLength(password)) {
            if (username.equals("admin") && password.equals("admin")) {
                HttpSession session = request.getSession(true);
                session.setAttribute(ApplicationVariable.USER_SESSION_KEY, new UserInfo(1, "admin"));
                return "<h1>登录成功！</h1>";
            }
        }
        return "<h1>用户名或密码错误！</h1>";
//        if (username != null && !username.equals("") && password != null && !password.equals("") && password.equals("admin")) {
//            //登录成功
//            return "<h1>登录成功！</h1>";
//        }else {
//            return"<h1>用户名或密码错误！</h1>";
//        }
    }

    @RequestMapping("/hello")
    public String sayHi() {
        //int a = 10 / 0;
        Integer num = Integer.valueOf(null);
        //System.out.println("执行了Controller方法");
        return "hello";
    }

    @RequestMapping(value = "/hi", method = RequestMethod.POST)
    public String hi() {
        return "hello spring mvc";
    }

    @GetMapping("/hi2")
    public String hi2(String name, @RequestParam("pwd") String password) {
        return "姓名：  " + name + " 密码： " + password;
    }

    @GetMapping("/hi3")
    public String hi3(@RequestParam String name, @RequestParam(value = "password", required = false) String password) {
        return "姓名：  " + name + " 密码： " + password;
    }

    @GetMapping("/add")
    public String add(@RequestBody User user) {
        return user.toString();
    }

//    @RequestMapping("/hello")
//    public String hello(User user) {
//        return " 姓名 " + user.getName() + " 密码 " + user.getPassword();
//    }


    //通过utl路径获取参数
    @RequestMapping("/loginbypath/{name}/{password}")
    public String loginByPath(@PathVariable String name, @PathVariable String password) {
        return "姓名：  " + name + " 密码： " + password;
    }

    //上传文件
    @RequestMapping("/upfile")
    public String upFile(String name, @RequestPart("photo") MultipartFile file) throws IOException {
        ApplicationHome applicationHome = new ApplicationHome(this.getClass());
        String path1 = applicationHome.getDir().getParentFile().getParentFile().getAbsolutePath();
        String path2 = "/src/main/resources/static/img/";
        String path3 = path1 + path2;
        //生成唯一的名称 UUID
        String filename = UUID.randomUUID().toString();
        String fileType = file.getOriginalFilename().substring(file.getOriginalFilename().lastIndexOf("."));
        filename += fileType;

        String path = path3 + filename;
        file.transferTo(new File(path));
        return "name: " + name + " 的图片上传成功！";

    }


    @RequestMapping("/cookie")
    public String getCookie2(@CookieValue(value = "name", required = false) String name) {
        return "cookie name :" + name;
    }

    @RequestMapping("/agent")
    public String getAgent(@RequestHeader("User-Agent") String userAgent) {
        return userAgent;
    }

    @RequestMapping("/setsession")
    public String setSession(String name, HttpServletRequest request) {
        HttpSession session = request.getSession(true);
        if (session != null) {
            session.setAttribute("name", name);
        }
        return "Session 设置成功！";
    }

    @RequestMapping("/getsession")
    public String getSesstion(@SessionAttribute(name = "name", required = false) String name) {
        return name;
    }

}


