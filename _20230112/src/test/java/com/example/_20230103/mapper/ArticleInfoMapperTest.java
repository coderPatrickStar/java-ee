package com.example._20230103.mapper;

import com.example._20230103.model.ArticleInfo;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.ArrayList;
import java.util.List;

@SpringBootTest
class ArticleInfoMapperTest {
    @Autowired
    private ArticleInfoMapper articleInfoMapper;

    @Test
    void updateArticle() {
        int result = articleInfoMapper.updateArticle(2, "Spring", null, null);
        System.out.println("受影响的行数： " + result);
    }

    @Test
    void getListByTitleOrUid() {
        List<ArticleInfo> list = articleInfoMapper.getListByTitleOrUid(null, 6);
        list.stream().forEach(c -> System.out.println(c));
    }

    @Test
    void getArticleByAId() {
        System.out.println(articleInfoMapper.getArticeByAId(1));
    }


}