package controller;

import model.Blog;
import model.BlogDao;
import model.User;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.WebContext;
import org.thymeleaf.templateresolver.ServletContextTemplateResolver;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.List;

@WebServlet("/blog_list.html")
public class BlogListServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.setContentType("text/html;charset=utf8");

        User user = Util.checkLogin(req);
        if (user == null) {
            resp.sendRedirect("blog_login.html");
            return;
        }

        BlogDao blogDao = new BlogDao();
        List<Blog> blogs = blogDao.selectAll();


        TemplateEngine engine = (TemplateEngine) getServletContext().getAttribute("engine");

        WebContext webContext = new WebContext(req, resp, getServletContext());
        webContext.setVariable("blogs", blogs);
        webContext.setVariable("user", user);
        engine.process("blog_list.html", webContext, resp.getWriter());
    }
}
