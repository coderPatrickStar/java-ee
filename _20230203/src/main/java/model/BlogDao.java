package model;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class BlogDao {
    public void insert(Blog blog) {
        Connection connection = null;
        PreparedStatement statement = null;
        try {
            connection = DBUtil.getConnection();
            String sql = "insert into blog values(null,?,?,?,?)";
            statement = connection.prepareStatement(sql);
            statement.setString(1, blog.getTitle());
            statement.setString(2, blog.getContent());
            statement.setTimestamp(3, blog.getPostTime());
            statement.setInt(4, blog.getUserId());
            int ret = statement.executeUpdate();
            if (ret != 1) {
                System.out.println("插入博客失败！");
            } else {
                System.out.println("插入博客成功！");
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        } finally {
            DBUtil.close(connection, statement, null);
        }
    }

    public List<Blog> selectAll() {
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        List<Blog> blogs = new ArrayList<>();

        try {
            connection = DBUtil.getConnection();
            String sql = "select * from blog order by postTime desc";
            statement = connection.prepareStatement(sql);
            resultSet = statement.executeQuery();
            while (resultSet.next()) {
                Blog blog = new Blog();
                blog.setBlogId(resultSet.getInt("blogId"));
                blog.setTitle(resultSet.getString("title"));

                String content = resultSet.getString("content");
                if (content.length() > 100) {
                    content = content.substring(0, 100) + "...";
                }
                blog.setContent(content);
                blog.setPostTime(resultSet.getTimestamp("postTime"));
                blog.setUserId(resultSet.getInt("userId"));
                blogs.add(blog);
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        } finally {
            DBUtil.close(connection, statement, resultSet);
        }
        return blogs;
    }

    public Blog selectOne(int blogId) {
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;

        try {
            connection = DBUtil.getConnection();
            String sql = "select * from blog where blogId = ?";
            statement = connection.prepareStatement(sql);
            statement.setInt(1, blogId);
            resultSet = statement.executeQuery();
            if (resultSet.next()) {
                Blog blog = new Blog();
                blog.setBlogId(resultSet.getInt("blogId"));
                blog.setTitle(resultSet.getString("title"));
                blog.setContent(resultSet.getString("content"));
                blog.setPostTime(resultSet.getTimestamp("postTime"));
                blog.setUserId(resultSet.getInt("userId"));
                return blog;
            }
            return null;

        } catch (SQLException e) {
            throw new RuntimeException(e);
        } finally {
            DBUtil.close(connection, statement, resultSet);
        }
    }

    public void delete(int blogId) {
        Connection connection = null;
        PreparedStatement statement = null;
        try {
            connection = DBUtil.getConnection();
            String sql = "delete from blog where blogId = ?";
            statement = connection.prepareStatement(sql);
            statement.setInt(1, blogId);
            int ret = statement.executeUpdate();
            if (ret != 1) {
                System.out.println("删除博客失败！");
            } else {
                System.out.println("删除博客成功！");
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        } finally {
            DBUtil.close(connection, statement, null);
        }
    }

    public static void main(String[] args) {
        BlogDao blogDao = new BlogDao();
        Blog blog = new Blog();
        blog.setBlogId(1);
        blog.setTitle("博客标题1");
        blog.setContent("博客正文1");
        blog.setPostTime(new Timestamp(System.currentTimeMillis()));
        blog.setUserId(1);
        blogDao.insert(blog);

        blog.setBlogId(2);
        blog.setTitle("博客标题2");
        blog.setContent("博客正文2");
        blog.setPostTime(new Timestamp(System.currentTimeMillis()));
        blog.setUserId(2);
        blogDao.insert(blog);

        blog.setBlogId(3);
        blog.setTitle("博客标题3");
        blog.setContent("博客正文3");
        blog.setPostTime(new Timestamp(System.currentTimeMillis()));
        blog.setUserId(1);
        blogDao.insert(blog);


//        List<Blog> blogs = blogDao.selectAll();
//        System.out.println(blogs);

//        Blog blog = blogDao.selectOne(1);
//        System.out.println(blog);
//
//        blogDao.delete(1);
//        blogDao.delete(3);
    }
}
