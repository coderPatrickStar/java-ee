//package com.example._20221228.test;
//
//public class NewCarExample {
//    public static void main(String[] args) {
//        Car car = new Car();
//        car.init();
//    }
//
//    //汽车对象
//    static class Car {
//        public void init() {
//            //依赖车身
//            FrameWork frameWork = new FrameWork();
//            frameWork.init();
//        }
//    }
//
//    //车身类
//    static class FrameWork {
//        public void init() {
//            //依赖底盘
//            Bottom bottom = new Bottom();
//            bottom.init();
//        }
//    }
//
//    //底盘类
//    static class Bottom {
//        public void init() {
//            //依赖轮胎
//            Tire tire = new Tire();
//            tire.init();
//        }
//    }
//
//    //轮胎类
//    static class Tire {
//        private int size = 30;
//
//        public void init() {
//            System.out.println("轮胎尺寸：" + size);
//        }
//    }
//}
