package com.example._20221228.test;

public class NewCarExample2 {
    public static void main(String[] args) {
        Car car = new Car(20);
        car.init();
    }

    //汽车对象
    static class Car {
        private FrameWork frameWork;

        public Car(int size) {
            frameWork = new FrameWork(size);
        }

        public void init() {
            //依赖车身
            frameWork.init();
        }
    }

    //车身类
    static class FrameWork {
        private Bottom bottom;

        public FrameWork(int size) {
            bottom = new Bottom(size);
        }

        public void init() {
            //依赖底盘
            bottom.init();
        }
    }

    //底盘类
    static class Bottom {
        private Tire tire;

        public Bottom(int size) {
            tire = new Tire(size);
        }

        public void init() {
            //依赖轮胎
            tire.init();
        }
    }

    //轮胎类
    static class Tire {
        private int size;

        public Tire(int size) {
            this.size = size;
        }

        public void init() {
            System.out.println("轮胎尺寸：" + size);
        }
    }
}
