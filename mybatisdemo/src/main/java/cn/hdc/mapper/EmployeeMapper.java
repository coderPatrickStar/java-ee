package cn.hdc.mapper;

import cn.hdc.pojo.Employee;

import java.util.List;

public interface EmployeeMapper {

    public List<Employee> selectByList(List<Integer> list);
    public List<Employee> findById(Integer id);

    public Integer updateEmployeeBySet(Employee employee);

    public List<Employee> findEmplloyeeByAgeAndPosition(Employee employee);

}
