package Test;

import cn.hdc.pojo.Employee;
import cn.hdc.pojo.Student;
import cn.hdc.utils.MyBatisUtils;
import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import org.junit.Test;

import java.io.IOException;
import java.io.Reader;
import java.util.ArrayList;
import java.util.List;

public class MyBatisTest {
    @Test
    public void selectByList() {
        SqlSession session = MyBatisUtils.getSession();
        List<Integer> list = new ArrayList<Integer>();
        list.add(2);
        list.add(3);
        List<Employee> employees = session.selectList("cn.hdc.mapper.EmployeeMapper.selectByList", list);
        for (Employee employee : employees) {
            System.out.println(employee);
        }
    }

    @Test
    public void findByIdTest() {
        SqlSession session = MyBatisUtils.getSession();
        Employee employee = session.selectOne("cn.hdc.mapper.EmployeeMapper.findById", 2);
        System.out.println(employee);
        session.close();
    }

    @Test
    public void findStudentByNameAndMajorTest() {
        //1获取sqlSession对象
        SqlSession session = MyBatisUtils.getSession();
        //2创建student对象 封装查询条件
        Student student = new Student();
        student.setName("张三");
        student.setMajor("英语");
        //3执行sql
        List<Student> students = session.selectList("cn.hdc.mapper.StudentMapper.findStudentByNameAndMajor", student);
        //4输出查询结果
        for (Student student1 : students) {
            System.out.println(student1);
        }

        session.close();
    }

    @Test
    public void updateEmployeeBySetTest() {
        String resources = "mybatis-config.xml";
        Reader reader = null;
        try {
            reader = Resources.getResourceAsReader(resources);
        } catch (IOException e) {
            e.printStackTrace();
        }
        SqlSessionFactory sqlMapper = new SqlSessionFactoryBuilder().build(reader);
        SqlSession session = sqlMapper.openSession();
        Employee employee = new Employee();
        employee.setId(1);
        employee.setAge(28);
        employee.setName("tom");
        employee.setPosition("副经理");
        int row = session.update("cn.hdc.mapper.EmployeeMapper.updateEmployeeBySet", employee);
        System.out.println(row);
        session.commit();
        session.close();
    }

    @Test
    public void testFindEmplloyeeByAgeAndPosition() {
        String resources = "mybatis-config.xml";
        Reader reader = null;
        try {
            reader = Resources.getResourceAsReader(resources);
        } catch (IOException e) {
            e.printStackTrace();
        }
        SqlSessionFactory sqlMapper = new SqlSessionFactoryBuilder().build(reader);
        SqlSession session = sqlMapper.openSession();
        Employee employee = new Employee();
        employee.setAge(20);
        employee.setPosition("员工");
        List<Employee> employees = session.selectList("cn.hdc.mapper.EmployeeMapper.findEmplloyeeByAgeAndPosition", employee);
        for (Employee employee1 : employees) {
            System.out.println(employee1);
        }
        session.close();
    }
}
