import com.hdc.beans.User;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.xml.XmlBeanFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.core.io.ClassPathResource;

public class App {
    public static void main(String[] args) {
//        //1.得到spring上下文对象
//        ApplicationContext applicationContext =
//                new ClassPathXmlApplicationContext("spring-config.xml");
//        //2.根据上下文对象获得一个bean
//        User user = (User) applicationContext.getBean("user");//根据 bean id 来获取
//
//        //3.使用bean
//        user.sayHi();

        //BeanFactory beanFactory = new XmlBeanFactory(new ClassPathResource("spring-config.xml"));
//        BeanFactory beanFactory1 = new ClassPathXmlApplicationContext("spring-config.xml");
//        User user = (User) beanFactory1.getBean("user");
//        user.sayHi();
//        User user1 = (User) beanFactory1.getBean("user1");
//        user1.sayHi();
//        User user2 = (User) beanFactory1.getBean("user2");
//        user2.sayHi();

        BeanFactory beanFactory1 = new ClassPathXmlApplicationContext("spring-config.xml");
        //方式一
        User user = (User) beanFactory1.getBean("user");
        user.sayHi();

//      //方式二
//        User user1 = beanFactory1.getBean(User.class);
//        user1.sayHi();

        //方式三
        User user2 = beanFactory1.getBean("user",User.class);
        user2.sayHi();
        User user3 = beanFactory1.getBean("user1",User.class);
        user3.sayHi();
        User user4 = beanFactory1.getBean("user2",User.class);
        user4.sayHi();
    }
}
