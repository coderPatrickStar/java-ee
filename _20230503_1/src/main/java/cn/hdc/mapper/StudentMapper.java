package cn.hdc.mapper;

import cn.hdc.pojo.Student;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import java.util.List;

public interface StudentMapper {
    @Select("select * from s_student where id=#{id}")
    public Student selectStudent(int id);

    @Update("update s_student set name=#{name},age=#{age} where id=#{id}")
    public int updateStudent(Student student);

    @Select("select * from s_student where cid = #{cid}")
    @Results({
            @Result(id = true,column = "id",property = "id"),
            @Result(column = "name",property = "name"),
            @Result(column = "age",property = "age")
    })
    public List<Student> selectStudentsByCid(int cid);
}
