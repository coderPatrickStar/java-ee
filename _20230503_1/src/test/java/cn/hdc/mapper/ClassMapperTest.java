package cn.hdc.mapper;

import cn.hdc.pojo.IClass;
import cn.hdc.utils.MybatisUtils;
import org.apache.ibatis.session.SqlSession;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class ClassMapperTest {
    private SqlSession session;

    @Before
    public void setUp() throws Exception {
        session = MybatisUtils.getSession();
    }

    @After
    public void tearDown() throws Exception {
        session.commit();
        session.close();
    }

    @Test
    public void selectClassById() {
        ClassMapper classMapper = session.getMapper(ClassMapper.class);
        IClass iClass = classMapper.selectClassById(2);
        System.out.println(iClass);
    }
}