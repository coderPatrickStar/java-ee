package com.example.exam_college1.controller;

import com.example.exam_college1.common.R;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.io.*;

@RestController
@RequestMapping("/common")
public class CommonController {

    @Value("${file.upload.path}")
    private String filePath;

    @Value("${file.download.path}")
    private String downloadFilePath;

    /**
     * 文件上传
     *
     * @param file
     * @return
     */
    @PostMapping("/upload")
    public R upload(MultipartFile file) {
        //获取上传文件的文件名
        String oldFileName = file.getOriginalFilename();

        String realFileName = oldFileName.substring(0, oldFileName.lastIndexOf("."));

        String extName = oldFileName.substring(oldFileName.lastIndexOf("."));

        System.out.println("当前文件上传的路径为:   " + filePath);

        //拼装新文件路径及文件名
        String newPath = filePath + realFileName + "_" + System.currentTimeMillis() + extName;

        File newFile = new File(newPath);

        //当路径不存在的时候，进行路径创建和补充
        if (!newFile.exists()) {
            newFile.mkdirs();
        }

        //文件上传
        try {
            file.transferTo(newFile);
        } catch (IOException e) {
            e.printStackTrace();
            return R.error().setData(e.getMessage());
        }

        return R.ok();
    }


    @GetMapping("/download")
    public String fileDownLoad(HttpServletResponse response, @RequestParam("fileName") String fileName){
        File file = new File(downloadFilePath +'/'+ fileName);
        if(!file.exists()){
            return "下载文件不存在";
        }
        response.reset();
        response.setContentType("application/octet-stream");
        response.setCharacterEncoding("utf-8");
        response.setContentLength((int) file.length());
        response.setHeader("Content-Disposition", "attachment;filename=" + fileName );

        try(BufferedInputStream bis = new BufferedInputStream(new FileInputStream(file));) {
            byte[] buff = new byte[1024];
            OutputStream os  = response.getOutputStream();
            int i = 0;
            while ((i = bis.read(buff)) != -1) {
                os.write(buff, 0, i);
                os.flush();
            }
        } catch (IOException e) {
            return "下载失败";
        }
        return "下载成功";
    }


}
