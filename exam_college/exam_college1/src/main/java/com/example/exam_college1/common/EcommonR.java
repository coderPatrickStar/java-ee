package com.example.exam_college1.common;


public enum EcommonR {
    OK(200, "响应成功"), NOTFOUND(404, "未找到"), ERROR(500, "系统异常");

    private Integer code;

    private String msg;

    EcommonR(Integer code, String msg) {
        this.code = code;
        this.msg = msg;
    }

    public Integer getCode() {
        return code;
    }

    public String getMsg() {
        return msg;
    }
}
