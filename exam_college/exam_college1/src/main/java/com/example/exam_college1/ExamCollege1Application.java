package com.example.exam_college1;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@MapperScan("com.example.exam_college1.mapper")
@SpringBootApplication
public class ExamCollege1Application {

    public static void main(String[] args) {
        SpringApplication.run(ExamCollege1Application.class, args);
    }

}
