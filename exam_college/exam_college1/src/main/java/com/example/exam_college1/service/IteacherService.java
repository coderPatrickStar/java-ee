package com.example.exam_college1.service;


import com.example.exam_college1.entity.Teacher;

import java.math.BigInteger;
import java.util.List;


public interface IteacherService {
    List<Teacher> queryList();

    Teacher queryOne(BigInteger id);
    Integer saveOne(Teacher teacher);
    Integer updateOne(Teacher teacher);
    Integer delOne(BigInteger id);
}
