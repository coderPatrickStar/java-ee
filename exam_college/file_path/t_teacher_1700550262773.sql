/*
Navicat MySQL Data Transfer

Source Server         : 本地5.7
Source Server Version : 50717
Source Host           : localhost:3306
Source Database       : smart_college

Target Server Type    : MYSQL
Target Server Version : 50717
File Encoding         : 65001

Date: 2023-11-14 08:17:45
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `t_teacher`
-- ----------------------------
DROP TABLE IF EXISTS `t_teacher`;
CREATE TABLE `t_teacher` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `name` varchar(255) DEFAULT NULL COMMENT '姓名',
  `age` int(11) DEFAULT NULL COMMENT '年龄',
  `sex` char(1) DEFAULT NULL COMMENT '性别',
  `position` varchar(255) DEFAULT NULL COMMENT '职位',
  `subject` varchar(255) DEFAULT NULL COMMENT '学科',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of t_teacher
-- ----------------------------
INSERT INTO `t_teacher` VALUES ('1', '李白', '18', '1', '学科主任', '语文');
INSERT INTO `t_teacher` VALUES ('17', '李四', '18', '0', '任课老师', '化学');
