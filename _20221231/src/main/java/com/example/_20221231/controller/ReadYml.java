package com.example._20221231.controller;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.PostConstruct;

@Controller
public class ReadYml {
    @Value("${string.value1}")
    private String str1;

    @Value("${string.value2}")
    private String str2;

    @Value("${string.value3}")
    private String str3;

    @PostConstruct
    public String getStr() {
        System.out.println(str1);
        System.out.println(str2);
        System.out.println(str3);
        return "";
    }

}
