package com.example._20221231.component;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

@Component
public class ReadProperties {
    @Value("${server.port}")
    private String port;

    @PostConstruct
    public void postContruct() {
        System.out.println("Read properties port:" + port);
    }
}
