package com.example.demo.controller;

import com.example.demo.model.DBTypes;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;

/**
 * Creat by TanLei
 * Description:
 * User:86183
 * Date:2022-06-16
 * Time:13:51
 */
@RequestMapping("/listconf")
@ResponseBody
@Controller
public class ListConfigConrtoller {

    @Resource
    private DBTypes dbTypes;

    @RequestMapping("/getList")
    public void getList(){
        System.out.println(dbTypes.getName().get(0));
    }
}
