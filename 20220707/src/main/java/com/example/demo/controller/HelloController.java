package com.example.demo.controller;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.annotation.WebServlet;
import java.util.HashMap;

/**
 * Creat by TanLei
 * Description:
 * User:86183
 * Date:2022-06-16
 * Time:13:51
 */
@Controller
//@WebServlet("/hello")
@ResponseBody
public class HelloController {
    @Value("${baidu.tocken}")
    private String baiduTocken;

//
//    @ResponseBody
//    @RequestMapping("/sayHi")
//    public String sayHi(){
//        return "<h1>hello spring boot!</h1>";
//
//    }
//    public HashMap<String,Object> sayHi(){
//        HashMap map = new HashMap();
//        map.put("name","java");
//        map.put("password","123456");
//        return map;
//    }

    @RequestMapping("/baidu")
    public String getBaiduTocken(){
        return baiduTocken;
    }
}
