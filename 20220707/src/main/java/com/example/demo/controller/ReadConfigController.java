package com.example.demo.controller;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * Creat by TanLei
 * Description:
 * User:86183
 * Date:2022-06-16
 * Time:13:51
 */
@Controller
@ResponseBody
public class ReadConfigController {
    @Value("${String.value}")
    private String myStringVal;

    @Value(("${myint}"))
    private Integer myInt;

    @Value("&{mynull.value}")
    private Object myNull;

    @RequestMapping("/mystr")
    public String getMyStringVal() {
        return myStringVal;
    }

    @RequestMapping("/myint")
    public int getMyInt() {
        return myInt;
    }

    @RequestMapping("mynull")
    public Object getMyNull() {
        return myNull;
    }

}
