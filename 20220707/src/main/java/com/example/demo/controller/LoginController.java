package com.example.demo.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;

/**
 * Creat by TanLei
 * Description:
 * User:86183
 * Date:2022-06-16
 * Time:13:51
 */
@Controller
@ResponseBody
@RequestMapping("/")
public class LoginController {
    @RequestMapping("/login")
    public String login(HttpServletRequest req) {
        String result="未知错误！";
        String username = req.getParameter("username");
        String password = req.getParameter("password");
        if (username != null && username.equals("") && password != null && !password.equals("")) {
            return "恭喜：登录成功！";
        } else {
            result = "非法参数！";
        }
        return result;

    }
}
