package com.example._20221231.controller;

import com.example._20221231.model.Student;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@ResponseBody
@RequestMapping("/stu")
public class StudentController {
    @Autowired
    private Student student;

    @RequestMapping("/id")
    public int getId() {
        return student.getId();
    }

    @RequestMapping("/name")
    public String getName() {
        return student.getName();
    }

    @RequestMapping("/age")
    public int getAge() {
        return student.getAge();
    }
}
