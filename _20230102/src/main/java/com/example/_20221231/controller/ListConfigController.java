package com.example._20221231.controller;

import com.example._20221231.model.DBTypes;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import java.util.List;

@Controller
@ResponseBody
@RequestMapping("/list")
public class ListConfigController {
    @Resource
    private DBTypes dbTypes;

    @RequestMapping("/getlist")
    public void getList() {
        System.out.println(dbTypes.getName().get(0));
    }
}
