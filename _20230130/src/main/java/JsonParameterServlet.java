import com.fasterxml.jackson.databind.ObjectMapper;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.InputStream;

class UserInfo {
    public String username;
    public String password;
}

@WebServlet("/jsonpara")
public class JsonParameterServlet extends HttpServlet {
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String body = readBody(req);
        System.out.println(body);
        ObjectMapper objectMapper = new ObjectMapper();
        UserInfo userInfo = objectMapper.readValue(body, UserInfo.class);
        resp.getWriter().write("username :" + userInfo.username);
        resp.getWriter().write("username :" + userInfo.password);
    }

    private String readBody(HttpServletRequest req) throws IOException {
        int contentlength = req.getContentLength();
        byte[] buffer = new byte[contentlength];
        InputStream inputStream = req.getInputStream();
        inputStream.read(buffer);
        return new String(buffer, "utf-8");
    }
}
