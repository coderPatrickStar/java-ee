import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

@WebServlet("/login")
public class LoginServlet extends HttpServlet {
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.setContentType("text/html;charset=utf8");
        String username = req.getParameter("username");
        String password = req.getParameter("password");
        if (username == null || "".equals(username) || password == null || "".equals(password)) {
            resp.getWriter().write("<h3>用户名或密码为空！</h3>");
            return;
        }
        if (!username.equals("admin") || (!password.equals("admin"))) {
            resp.getWriter().write("<h3>用户名或者密码错误</h3>");
            return;
        }
        HttpSession session = req.getSession(true);
        session.setAttribute("visitCount", 0);
        resp.sendRedirect("index");
    }
}
