package com.hdc.beans;

import org.springframework.stereotype.Service;

/**
 * Creat by TanLei
 * Description:
 * User:86183
 * Date:2022-06-16
 * Time:13:51
 */
@Service
public class UserService {
    public void sayHi(String name){
        System.out.println("你好："+name);
    }
}
