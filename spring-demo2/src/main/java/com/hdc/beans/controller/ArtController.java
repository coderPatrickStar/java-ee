package com.hdc.beans.controller;

import com.hdc.beans.service.ArtService;
import com.hdc.model.UserInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import javax.annotation.Resource;

/**
 * Creat by TanLei
 * Description:
 * User:86183
 * Date:2022-06-16
 * Time:13:51
 */
@Controller
public class ArtController {
//    @Autowired
//    private ArtService artService;
//    public void addArt(String title,String content){
//        if(title!=null && content!=null && !title.equals("") && !content.equals("")){
//            //传参正常调用后面的业务场景
//            artService.addArt(title,content);
//        }else {
//            System.out.println("添加文章失败：前端传递了无效的参数！");
//        }
//    }
    private ArtService artService;
    @Autowired
    public ArtController(ArtService artService){
        this.artService=artService;

    }
    public ArtController(ArtService artService,String name){
        this.artService=artService;

    }
    public void addArt(String title,String content){
        if(title!=null && content!=null && !title.equals("") && !content.equals("")){
            //传参正常调用后面的业务场景
            artService.addArt(title,content);
        }else {
            System.out.println("添加文章失败：前端传递了无效的参数！");
        }
    }
    //注入方式3：使用setter进行注入
//    private ArtService artService;
//    @Autowired
//    public void setArtService(ArtService artService){
//        this.artService=artService;
//    }
//
//    public void addArt(String title,String content){
//        if(title != null && content != null && !title.equals("") && !content.equals("")){
//            artService.addArt(title,content);
//        }else {
//            System.out.println("添加文章失败：前端传递了非法参数！");
//        }
//
//    }
//    @Resource
//    private ArtService artService2;
//    @Resource(name = "user2")
//    private UserInfo userInfo;
//
//    public void addArt(String title,String content){
//        System.out.println(userInfo);
////        if(title != null && content != null && !title.equals("") && !content.equals("")){
////            artService2.addArt(title,content);
////        }else {
////            System.out.println("添加文章失败：前端传递了非法参数！");
////        }
//
//    }
}
