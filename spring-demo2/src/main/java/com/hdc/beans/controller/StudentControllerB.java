package com.hdc.beans.controller;

import com.hdc.model.Student;
import org.springframework.stereotype.Controller;

import javax.annotation.Resource;

/**
 * Creat by TanLei
 * Description:
 * User:86183
 * Date:2022-06-16
 * Time:13:51
 */
@Controller
public class StudentControllerB {
    @Resource
    private Student student;

    public Student getStudent() {
        return student;
    }
}
