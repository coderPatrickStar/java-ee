package com.hdc.beans.controller;

import com.hdc.beans.component.StudentComponent;
import com.hdc.model.Student;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

/**
 * Creat by TanLei
 * Description:
 * User:86183
 * Date:2022-06-16
 * Time:13:51
 */
@Controller
public class StudentControllerA {
    @Autowired
    private Student student;

    public Student getStudent(){
        Student thisStudent = student;
        System.out.println("Student 原始数据："+thisStudent);
        System.out.println("");
        System.out.println("修改student信息");

        thisStudent.setName("八戒");
        return thisStudent;
    }
}
