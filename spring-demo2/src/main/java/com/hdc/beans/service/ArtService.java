package com.hdc.beans.service;

import org.springframework.stereotype.Service;

/**
 * Creat by TanLei
 * Description:
 * User:86183
 * Date:2022-06-16
 * Time:13:51
 */
@Service
public class ArtService {

    public void addArt(String title,String content){
        //调用后面的流程
        System.out.println("执行了文章的service方法！"+title+" "+content);
    }
}
