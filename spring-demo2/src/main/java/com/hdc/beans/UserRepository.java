package com.hdc.beans;

import org.springframework.stereotype.Repository;

import javax.annotation.Resource;

/**
 * Creat by TanLei
 * Description:
 * User:86183
 * Date:2022-06-16
 * Time:13:51
 */
@Repository
public class UserRepository {
    public void sayHi(){
        System.out.println("你好，repository!");
    }
}
