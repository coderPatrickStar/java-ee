package com.hdc.beans.component;

import org.springframework.beans.factory.BeanNameAware;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;

/**
 * Creat by TanLei
 * Description:
 * User:86183
 * Date:2022-06-16
 * Time:13:51
 */
@Component
public class BeanLIfe implements BeanNameAware {
    @Override
    public void setBeanName(String s) {
        System.out.println("执行了 BeanNameAware setBeanName 方法："+s);
    }
    @PostConstruct
    public void postConstruct(){
        System.out.println("执行：@PostConstruct");
    }
    public void init(){
        System.out.println("执行：init-method");
    }
    @PreDestroy
    public void PreDestroy(){
        System.out.println("执行：@PreDestroy");
    }
    public void myDestory(){
        System.out.println("执行：myDestory ");
    }
}
