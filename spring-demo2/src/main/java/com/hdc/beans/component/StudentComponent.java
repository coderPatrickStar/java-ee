package com.hdc.beans.component;

import com.hdc.model.Student;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

/**
 * Creat by TanLei
 * Description:
 * User:86183
 * Date:2022-06-16
 * Time:13:51
 */
@Component
public class StudentComponent {
    @Scope("prototype")
    @Bean(name = "student")
    public Student StudentNO1(){
        Student student = new Student();
        student.setId(1);
        student.setName("孙悟空");
        student.setPassword("齐天大圣");
        student.setAge(10000);
        return student;
    }
}
