package com.hdc.beans;

import org.springframework.stereotype.Component;

/**
 * Creat by TanLei
 * Description:
 * User:86183
 * Date:2022-06-16
 * Time:13:51
 */
@Component
public class UserComponent {
    public void sayHi(){
        System.out.println("你好：component!");
    }
}
