package com.hdc.beans;

import com.hdc.model.UserInfo;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;

/**
 * Creat by TanLei
 * Description:
 * User:86183
 * Date:2022-06-16
 * Time:13:51
 */
@Component
public class UserBeans {
    @Bean(name = {"userInfo","user"})
    public UserInfo user(){
        UserInfo userInfo = new UserInfo();
        userInfo.setId(1);
        userInfo.setName("Java");
        userInfo.setPassword("123456");
        return userInfo;
    }
    @Bean(name = {"userInfo","user"})
    public UserInfo user1(){
        UserInfo userInfo = new UserInfo();
        userInfo.setId(2);
        userInfo.setName("spring");
        userInfo.setPassword("123456");
        return userInfo;
    }
}
