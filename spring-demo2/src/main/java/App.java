import com.hdc.beans.*;
import com.hdc.beans.component.BeanLIfe;
import com.hdc.beans.controller.ArtController;
import com.hdc.beans.controller.StudentControllerA;
import com.hdc.beans.controller.StudentControllerB;
import com.hdc.model.Student;
import com.hdc.model.UserInfo;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * Creat by TanLei
 * Description:
 * User:86183
 * Date:2022-06-16
 * Time:13:51
 */
public class App {
    public static void main(String[] args) {
        ApplicationContext context = new ClassPathXmlApplicationContext("spring-config.xml");
//        UserController userController = context.getBean("userController",UserController.class);
//        userController.sayHi();
//
//        UserService userService = context.getBean("userService",UserService.class);
//        userService.sayHi("tanlei");
//
//        UserRepository userRepository = context.getBean("userRepository",UserRepository.class);
//        userRepository.sayHi();
//
//        UserComponent userComponent = context.getBean("userComponent",UserComponent.class);
//        userComponent.sayHi();
//
//        UserConfiguration userConfiguration = context.getBean("userConfiguration",UserConfiguration.class);
//        userConfiguration.showConfig();
//
//        UService uService = context.getBean("UService",UService.class);
//        uService.sayHi();
//
//        UserInfo userInfo = context.getBean("userInfo",UserInfo.class);
//        UserInfo userInfo1 = context.getBean("user",UserInfo.class);
//        System.out.println(userInfo);
//        System.out.println(userInfo1);
//
//        ArtController artController = context.getBean("artController",ArtController.class);
//        artController.addArt("详解spring中的常用注解","详解spring中的正文");

//        StudentControllerA studentControllerA = context.getBean("studentControllerA",StudentControllerA.class);
//        Student studentA = studentControllerA.getStudent();
//        System.out.println("ControllerA:"+studentA);
//        System.out.println();
//
//        StudentControllerB studentControllerB = context.getBean("studentControllerB",StudentControllerB.class);
//        Student studentB = studentControllerB.getStudent();
//        System.out.println("ControllerB:"+studentB);

        BeanLIfe beanLIfe = context.getBean("beanLIfe",BeanLIfe.class);
        System.out.println("使用Bean");
        System.out.println("销毁Bean");
    }
}
