package com.example._20230103.mapper;

import com.example._20230103.model.UserInfo;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

@SpringBootTest
class ArticleInfoMapper2Test {
    @Autowired
    private ArticleInfoMapper2 articleInfoMapper2;

    @Test
    void getUserById() {
        System.out.println(articleInfoMapper2.getArticleById(1));
    }
}