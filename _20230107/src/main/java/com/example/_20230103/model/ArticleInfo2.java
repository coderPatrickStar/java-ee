package com.example._20230103.model;

import lombok.Data;

import java.time.LocalDateTime;

@Data
public class ArticleInfo2 {
    private int id;
    private String titlename;
    private String content;
    private LocalDateTime createtime;
    private LocalDateTime updatetime;
    private int uid;
    private int rcount;
    private int state;
    private UserInfo userInfo;
}
