package com.example._20230103.mapper;

import com.example._20230103.model.ArticleInfo;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface ArticleInfoMapper {
    public ArticleInfo getArticeByAId(int id);
}
