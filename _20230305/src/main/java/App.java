import com.hdc.beans.Bean1;
import com.hdc.beans.User1;
import com.hdc.beans.User2;
import com.hdc.controller.UserController;
import com.hdc.service.OrderService;
import com.hdc.service.impl.OrderServiceImpl;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.xml.XmlBeanFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.FileSystemResource;

public class App {
    public static void main(String[] args) {
        ApplicationContext context = new ClassPathXmlApplicationContext("application.xml");
        User2 user2 = (User2) context.getBean("user2");
        System.out.println(user2);


        //        Bean1 bean1 = (Bean1) context.getBean("bean1");
//        System.out.println(bean1);
//        Bean1 bean2 = (Bean1) context.getBean("bean2");
//        System.out.println(bean2);
        //        User1 user1 = (User1) context.getBean("user1");
//        System.out.println(user1);


        //        UserController userController = (UserController) context.getBean("userController");
//        userController.save();

        //        User2 user2 = (User2) context.getBean("user2");
//        System.out.println(user2);


        //        User1 user1 = (User1) context.getBean("user1");
//        System.out.println(user1);


        //        Object bean1 = context.getBean("bean1");
//        Object bean1_1 = context.getBean("bean1");
//        System.out.println(bean1);
//        System.out.println(bean1_1);
//        System.out.println(bean1 == bean1_1);


        //        Object bean4 = context.getBean("bean4");
//        System.out.println(bean4);


        //        Object bean3 = context.getBean("bean3");
//        System.out.println(bean3);


        //        Object bean2 = context.getBean("bean2");
//        System.out.println(bean2);


//        Object bean1 = context.getBean("bean1");
//        Object bean2 = context.getBean("bean2");
//        System.out.println(bean1);
//        System.out.println(bean2);


//        Object bean1 = context.getBean("bean1");
//        Object bean1_1 = context.getBean("bean1");
//        Object bean2 = context.getBean("bean2");
//        Object bean2_2 = context.getBean("bean2");
//
//        System.out.println(bean1);
//        System.out.println(bean1_1);
//        System.out.println(bean2);
//        System.out.println(bean2_2);


        //BeanFactory默认是第一次获取对象的时候，创建对象
        //       BeanFactory beanFactory = new XmlBeanFactory(new FileSystemResource("D:\\workespace\\workespace_javaee\\java-ee\\_20230305\\src\\main\\resources\\application.xml"));
//        OrderServiceImpl orderService = (OrderServiceImpl) beanFactory.getBean("OrderService");
//        orderService.hello();

//        OrderService orderService1 = (OrderService) beanFactory.getBean("OrderService");
//        orderService1.hello();

        //是beanfactory的子接口，在容器初始化的时候，就创建了bean。
    }
}
