package com.hdc.factory;

import com.hdc.beans.Bean2;

public class MyBeanFactory {
    public static Bean2 createBean() {
        System.out.println("工厂的静态方法！");
        return new Bean2();
    }
}
