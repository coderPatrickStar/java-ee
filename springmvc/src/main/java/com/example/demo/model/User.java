package com.example.demo.model;

import lombok.Data;

/**
 * Creat by TanLei
 * Description:
 * User:86183
 * Date:2022-06-16
 * Time:13:51
 */
@Data
public class User {
    private int id;
    private String name;
    private String password;
    private int age;
}
