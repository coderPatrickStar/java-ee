package com.example.demo.controller;

import com.example.demo.model.User;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

/**
 * Creat by TanLei
 * Description:
 * User:86183
 * Date:2022-06-16
 * Time:13:51
 */
@Controller
@RequestMapping("/user")
@ResponseBody
public class UserController {
    @RequestMapping("/hi")
    public String hi() {
        return "hello,spring mvc111111.";
    }

    @GetMapping("/hi2")
    public String hi2(String name, String password) {
        //return "hello springmvc2";
        return "hello " + name + " | password= " + password;
    }

    @PostMapping("/hi3")
    public String hi3() {
        return "hello spring mvc 3";
    }

    @RequestMapping("/add")
    public String add(User user) {
        return user.toString();
    }

    @RequestMapping("/login")
    public String login(String name,@RequestParam("pwd") String password) {
        return "name= " + name + " | password " + password;
    }

    @RequestMapping("/login2")
    public String login2(@RequestParam(required = false) String name,@RequestParam String pwd){
        return "name= " + name + " | password " + pwd;
    }
}
