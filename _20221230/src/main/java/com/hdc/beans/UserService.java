//package com.hdc.beans;
//
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.stereotype.Controller;
//import org.springframework.stereotype.Service;
//
//import javax.annotation.PostConstruct;
//
//@Service
//public class UserService {
//    public UserService() {
//        System.out.println("调用 UserService 构造方法");
//    }
//
//    public void sayHi(String name) {
//        System.out.println("hello " + name);
//    }
//}
//
//
//@Controller
//public class UserController {
//    @Autowired
//    private UserService userService;
//
//    @PostConstruct
//    public UserController(UserService userService) {
//        userService.sayHi();
//        System.out.println("执行 UserController 构造方法");
//    }
//}