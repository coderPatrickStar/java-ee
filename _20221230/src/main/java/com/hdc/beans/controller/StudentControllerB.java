package com.hdc.beans.controller;

import com.hdc.model.Student;
import org.springframework.stereotype.Controller;

import javax.annotation.Resource;

@Controller
public class StudentControllerB {
    @Resource
    private Student student;

    public Student getStudent() {
        return student;
    }
}
