package com.hdc.beans.component;

import org.springframework.beans.factory.BeanNameAware;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;

@Component
public class BeanLife implements BeanNameAware {
    @Override
    public void setBeanName(String s) {
        System.out.println("执行了 BeanNameAware setBeanName方法：" + s);
    }

    //初始化方式一
    @PostConstruct
    public void postConstruct() {
        System.out.println("执行：postConstruct");
    }

    //初始化方式二
    public void init() {
        System.out.println("执行 init 方法");
    }

    //销毁方法一
    @PreDestroy
    public void preDestory() {
        System.out.println("执行 preDestory 方法");
    }

    //销毁方法二
    public void myDestory() {
        System.out.println("执行 myDestory 方法");
    }
}
