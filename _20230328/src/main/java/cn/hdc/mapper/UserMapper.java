package cn.hdc.mapper;

import cn.hdc.pojo.User;

/**
 * 接口式开发
 * 1.方法的名称需要保证和映射文件中的sql语句的statmentId一致
 */
public interface UserMapper {
    public User findById(int uid);

    public Integer findTotal();

    public void addUser(User user);

    public void updateUser(User user);
}
