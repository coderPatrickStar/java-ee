package cn.hdc.mapper;

import cn.hdc.pojo.Student;

import java.util.List;

public interface StudentMapper {
    public List<Student> findAllStudent();
}
