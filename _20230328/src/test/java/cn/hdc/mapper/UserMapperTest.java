package cn.hdc.mapper;

import cn.hdc.pojo.User;
import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import org.junit.Before;
import org.junit.Test;

import java.io.InputStream;

import static org.junit.Assert.*;

public class UserMapperTest {
    UserMapper userMapper = null;
    SqlSession sqlSession = null;

    @Before
    public void setUp() throws Exception {
        InputStream inputStream = Resources.getResourceAsStream("mybatis-config.xml");
        SqlSessionFactory sqlSessionFactory = new SqlSessionFactoryBuilder().build(inputStream);
        sqlSession = sqlSessionFactory.openSession();
        userMapper = sqlSession.getMapper(UserMapper.class);
    }

    @Test
    public void findById() {
        User user = userMapper.findById(1);
        System.out.println(user);
        sqlSession.close();
    }

    @Test
    public void findTotal() {
        Integer total = userMapper.findTotal();
        System.out.println(total);
        sqlSession.close();
    }

    @Test
    public void addUser() {
        User user = new User();
        user.setUage(23);
        user.setUname("wangwu");
        System.out.println("数据插入前的用户id:" + user.getUid());
        userMapper.addUser(user);
        System.out.println("数据插入后的用户id:" + user.getUid());
        sqlSession.commit();
        sqlSession.close();
    }

    @Test
    public void updateUser() {
        User user = new User();
        user.setUid(6);
        user.setUname("王五");
        user.setUage(30);
        userMapper.updateUser(user);
        sqlSession.commit();
        sqlSession.close();
    }
}