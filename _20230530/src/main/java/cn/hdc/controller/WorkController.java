package cn.hdc.controller;

import cn.hdc.pojo.Work;
import cn.hdc.utils.DBUtil;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.Map;

@Controller
public class WorkController {
    @RequestMapping("/mark")
    @ResponseBody
    public Work workMark(Work work) {
        DBUtil.markInfo.put("work", work);
        DBUtil.markInfo.put("total", DBUtil.getTotal() + 1);
        return work;
    }

    @RequestMapping("/total")
    @ResponseBody
    public Map<String, Object> getTotal() {
        return DBUtil.markInfo;
    }
}
