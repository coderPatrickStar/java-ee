package cn.hdc.controller;

import cn.hdc.pojo.User;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.Date;

@Controller
public class UserController {
    @RequestMapping("/findById")
    public String findById(HttpServletRequest request,
                           HttpServletResponse response,
                           HttpSession session,
                           Model model) {
        System.out.println(response);
        System.out.println(request);
        //向模型中存放数据
        model.addAttribute("msg", "你好");
        //获取请求中参数
        String userid = request.getParameter("userid");
        System.out.println("根据ID查询用户信息" + userid);
        return "success";
    }

    @RequestMapping("/get")
    public String getUserNameAndId(String username, Integer id) {
        System.out.println("用户名：" + username + " ID:" + id);
        return "success";
    }

    @RequestMapping("/getUsername")
    public String getUsername(@RequestParam(value = "name", defaultValue = "hdc") String username) {
        System.out.println("用户名：" + username);
        return "success";
    }

    @RequestMapping("/user/{name}")
    public String getPathVariable(@PathVariable("name") String username) {
        System.out.println(username);
        return "success";
    }

    @RequestMapping("/registerUser")
    public String registerUser(User user) {
        System.out.println(user);
        return "success";
    }
//    基于XML配置自定义转换器实现
//    @RequestMapping("/getBirthday")
//    public String getBirthday(Date birthday) {
//        System.out.println(birthday);
//        return "success";
//    }

    //    使用@DateTimeFormat注解完成日期类型的格式转换无需自定义转换器
    @RequestMapping("/getBirthday")
    public String getBirthday(@DateTimeFormat(pattern = "yyyy-MM-dd") Date birthday) {
        System.out.println(birthday);
        return "success";
    }

//    @RequestMapping("/findOrderWithUser")
//    public String findOrderWithUser(User user) {
//        System.out.println("用户民" + user.getUsername());
//        System.out.println("订单号" + user.getOrder().getOrderId());
//        return "success";
//    }
}
