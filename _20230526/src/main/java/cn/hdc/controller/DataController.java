package cn.hdc.controller;

import cn.hdc.pojo.Product;
import cn.hdc.pojo.User;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@Controller
public class DataController {
    @RequestMapping("/showDataByResponse")
    public void showDataByResponse(HttpServletResponse response) throws IOException {
        response.getWriter().write("success");
    }

//    @RequestMapping("/showDataByJson")
//    public void showDataByJson(HttpServletResponse response) throws IOException {
//        User user = new User();
//        user.setUsername("tom");
//        user.setPassword("12346");
//        ObjectMapper objectMapper = new ObjectMapper();
//        String json = objectMapper.writeValueAsString(user);
//        response.getWriter().write(json);
//    }

    @RequestMapping("/showDataByJson")
    @ResponseBody
    public void showDataByJson(HttpServletResponse response) throws IOException {
        User user = new User();
        user.setUsername("tom");
        user.setPassword("12346");
        ObjectMapper objectMapper = new ObjectMapper();
        String json = objectMapper.writeValueAsString(user);
        response.getWriter().write(json);
    }

    @RequestMapping("/getUser")
    @ResponseBody
    public User getUser() {
        User user = new User();
        user.setUsername("tom");
        user.setPassword("12346");
        return user;
    }

    @RequestMapping("/addProducts")
    @ResponseBody
    public List<Product> addProduct() {
        List<Product> productList = new ArrayList<>();
        Product product = new Product();
        product.setProId("p001");
        product.setProName("三文鱼");
        Product product1 = new Product();
        product1.setProId("p002");
        product1.setProName("茅台");
        productList.add(product);
        productList.add(product1);
        return productList;
    }
}
