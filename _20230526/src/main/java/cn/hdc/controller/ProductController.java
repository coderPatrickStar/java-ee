package cn.hdc.controller;

import cn.hdc.pojo.Product;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

@Controller
public class ProductController {
//    @RequestMapping("/getProducts")
//    public String getProducts(String[] proIds) {
//        for (String proId : proIds) {
//            System.out.println("商品的id为：" + proId);
//        }
//        return "success";
//    }

//    @RequestMapping("/getProducts")
//    public String getProducts(@RequestParam("proIds") List<String> proIds) {
//        for (String proId : proIds) {
//            System.out.println("商品的id为：" + proId);
//        }
//        return "success";
//    }

    @RequestMapping("/getProduct")
    public String getProduct(@RequestBody Product product) {
        System.out.println("单个商品：" + product);
        return "success";
    }

    @RequestMapping("/getProductList")
    public String getProductList(@RequestBody List<Product> productList) {
        System.out.println("多个商品：");
        productList.forEach(product -> {
            System.out.println(product);
        });
        return "success";
    }
}
