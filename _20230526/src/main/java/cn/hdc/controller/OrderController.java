package cn.hdc.controller;

import cn.hdc.pojo.Order;
import cn.hdc.pojo.Product;
import cn.hdc.pojo.User;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.HashMap;
import java.util.List;
import java.util.Set;

@Controller
public class OrderController {
//    @RequestMapping("/showOrders")
//    public String showOrders(User user) {
//        List<String> address = user.getAddress();
//        List<Order> orders = user.getOrders();
//
//        for (int i = 0; i < orders.size(); i++) {
//            Order order = orders.get(i);
//            String addr = address.get(i);
//            System.out.println("订单编号：" + order.getOrderId() + "订单名称：" + order.getOrderName() + " 配送地址：" + addr);
//        }
//        return "success";
//    }

    @RequestMapping("/orderInfo")
    public String getOrderInfo(Order order) {
        System.out.println("订单编号：" + order.getOrderId());
        HashMap<String, Product> productInfo = order.getProductInfo();
        Set<String> keys = productInfo.keySet();
        for (String type : keys) {
            Product product = productInfo.get(type);
            System.out.println("类型：" + type + "商品名称" + product.getProName());
        }
        return "success";
    }
}
