package cn.hdc.controller;

import cn.hdc.pojo.User;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;

@Controller
public class PageController {
    @RequestMapping("/register")
    public void showPageByVoid() {
        System.out.println("showPageByVoid running");
    }

    @RequestMapping("/showPageByString")
    public String showPageByString() {
        System.out.println("showPageByString running");
        return "register";
    }

    @RequestMapping("/showPageByForward")
    public String showPageByForward() {
        System.out.println("showPageByForward running");
//        视图解析器不在生效
        return "forward:static/order.jsp";
    }

    @RequestMapping("/showPageByRedirect")
    public String showPageByRedirect() {
        System.out.println("showPageByRedirect running");
        //        视图解析器不在生效
        return "redirect:https://www.baidu.com";
    }

    @RequestMapping("/showPageByRequest")
    public String showPageByRequest(HttpServletRequest request) {
        System.out.println("showPageByRequest running");
        request.setAttribute("username", "Tom");
        return "register";
    }

    @RequestMapping("/showPageByModel")
    public String showPageByModel(Model model) {
        System.out.println("showPageByModel running");
        model.addAttribute("username", "Tom");
        return "register";
    }

    @RequestMapping("/showModelAndView")
    public ModelAndView showModelAndView() {
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.addObject("username", "Tom");
        User user = new User();
        user.setPassword("123456");
        modelAndView.addObject("user", user);
        modelAndView.setViewName("register");
        return modelAndView;
    }
}
