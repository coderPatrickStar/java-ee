package cn.hdc.beans;

import cn.hdc.entity.User;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;

@Component
public class UserBeans {
    @Bean(name = "userbean")
    public User get_User() {
        User user = new User();
        user.setName("zhangsan");
        return user;
    }
}
