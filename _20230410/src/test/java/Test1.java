import cn.hdc.pojo.Customer;
import cn.hdc.utils.MybatisUtils;
import org.apache.ibatis.session.SqlSession;
import org.junit.Test;

import java.util.List;

public class Test1 {
    @Test
    public void findCustomerByUsernameAndJobsTest() {
        SqlSession session = MybatisUtils.getSession();
        Customer customer = new Customer();
        //customer.setUsername("jack");
        customer.setJobs("teacher");
        List<Customer> customers = session.selectList("cn.hdc.pojo.Customer.findCustomerByUsernameAndJobs", customer);
        for (Customer c : customers) {
            System.out.println(c);
        }
        session.close();
    }
}
