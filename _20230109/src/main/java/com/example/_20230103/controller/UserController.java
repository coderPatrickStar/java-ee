package com.example._20230103.controller;

import com.example._20230103.model.User;
import com.example._20230103.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.system.ApplicationHome;
import org.springframework.core.io.ClassPathResource;
import org.springframework.stereotype.Controller;
import org.springframework.util.ClassUtils;
import org.springframework.util.ResourceUtils;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.*;
import java.io.File;
import java.io.IOException;
import java.util.UUID;

@Controller
@RequestMapping("/user")
@ResponseBody   //当前类中所有方法，将返回一个非静态页面的数据
public class UserController {
    @Autowired
    private UserService userService;

    @RequestMapping("/hello")
    public String sayHi() {
        System.out.println("执行了Controller方法");
        return "hello";
    }

    @RequestMapping(value = "/hi", method = RequestMethod.POST)
    public String hi() {
        return "hello spring mvc";
    }

    @GetMapping("/hi2")
    public String hi2(String name, @RequestParam("pwd") String password) {
        return "姓名：  " + name + " 密码： " + password;
    }

    @GetMapping("/hi3")
    public String hi3(@RequestParam String name, @RequestParam(value = "password", required = false) String password) {
        return "姓名：  " + name + " 密码： " + password;
    }

    @GetMapping("/add")
    public String add(@RequestBody User user) {
        return user.toString();
    }

//    @RequestMapping("/hello")
//    public String hello(User user) {
//        return " 姓名 " + user.getName() + " 密码 " + user.getPassword();
//    }


    //通过utl路径获取参数
    @RequestMapping("/loginbypath/{name}/{password}")
    public String loginByPath(@PathVariable String name, @PathVariable String password) {
        return "姓名：  " + name + " 密码： " + password;
    }

    //上传文件
    @RequestMapping("/upfile")
    public String upFile(String name, @RequestPart("photo") MultipartFile file) throws IOException {
        ApplicationHome applicationHome = new ApplicationHome(this.getClass());
        String path1 = applicationHome.getDir().getParentFile().getParentFile().getAbsolutePath();
        String path2 = "/src/main/resources/static/img/";
        String path3 = path1 + path2;
        //生成唯一的名称 UUID
        String filename = UUID.randomUUID().toString();
        String fileType = file.getOriginalFilename().substring(file.getOriginalFilename().lastIndexOf("."));
        filename += fileType;

        String path = path3 + filename;
        file.transferTo(new File(path));
        return "name: " + name + " 的图片上传成功！";

    }


    @RequestMapping("/cookie")
    public String getCookie2(@CookieValue(value = "name", required = false) String name) {
        return "cookie name :" + name;
    }

    @RequestMapping("/agent")
    public String getAgent(@RequestHeader("User-Agent") String userAgent) {
        return userAgent;
    }

    @RequestMapping("/setsession")
    public String setSession(String name, HttpServletRequest request) {
        HttpSession session = request.getSession(true);
        if (session != null) {
            session.setAttribute("name", name);
        }
        return "Session 设置成功！";
    }

    @RequestMapping("/getsession")
    public String getSesstion(@SessionAttribute(name = "name", required = false) String name) {
        return name;
    }

}


