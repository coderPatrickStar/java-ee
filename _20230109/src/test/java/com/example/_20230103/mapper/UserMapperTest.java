package com.example._20230103.mapper;

import com.example._20230103.model.UserInfo;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
class UserMapperTest {
    @Autowired
    private UserMapper userMapper;

    @Test
    void delIds() {
        List<Integer> list = new ArrayList<>();
        list.add(6);
        list.add(7);
        int result = userMapper.delIds(list);
        System.out.println("受影响行数：" + result);
    }

    @Test
    void addUser4() {
        int result = userMapper.addUser4("Servlet", "Servlet", null, null);
        System.out.println("受影响的行数： " + result);
    }

//    @Test
//    void addUser3() {
//        int result = userMapper.addUser3("MyBatis", "MyBatis", "a.png");
//        System.out.println("受影响的行数： " + result);
//    }

    @Test
    void addUser3() {
        int result = userMapper.addUser3("Java", "Java", null);
        System.out.println("受影响的行数： " + result);
    }

    @Test
    void getUserById() {
        System.out.println(userMapper.getUserById(1));
    }

    @Test
    void addUser() {
        int flag = userMapper.addUser("admin", "admin");
        System.out.println("受影响的行数 " + flag);
    }

    @Test
    void addUser2() {
        UserInfo userInfo = new UserInfo();
        userInfo.setUsername("admin");
        userInfo.setPassword("admin");
        userMapper.addUser2(userInfo);
        System.out.println("自增id " + userInfo.getId());
    }

    @Test
    void updateUser() {
        UserInfo userInfo = new UserInfo();
        userInfo.setUsername("abc");
        userInfo.setPassword("abc");
        userInfo.setId(1);
        int flag = userMapper.updateUser(userInfo);
        System.out.println("受影响的行数： " + flag);
    }

    @Test
    void deleteUser() {
        UserInfo userInfo = new UserInfo();
        userInfo.setId(1);
        int flag = userMapper.deleteUser(userInfo);
        System.out.println("受影响的行数： " + flag);
    }

    @Test
    void getAllBySort() {
        UserInfo userInfo = new UserInfo();
        List<UserInfo> list = userMapper.getAllBySort("desc");
        list.stream().forEach(u -> System.out.println(u));
    }

    @Test
    void isLogin() {
        String username = "tanlei";
        String password = "666";
        String sql_injection_password = "' or 1='1";
        UserInfo userInfo = new UserInfo();
        System.out.println(userMapper.isLogin(username, password));
        System.out.println(userMapper.isLogin(username, sql_injection_password));
    }

    @Test
    void getAllBylike() {
        String name = "tan";
        List<UserInfo> list = userMapper.getAllBylike(name);
        list.stream().forEach(u -> System.out.println(u));
    }

    @Test
    void getUserById2() {
        UserInfo userInfo = userMapper.getUserById2(6);
        System.out.println(userInfo);
    }
}
