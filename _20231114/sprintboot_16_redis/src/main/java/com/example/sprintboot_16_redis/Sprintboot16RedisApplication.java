package com.example.sprintboot_16_redis;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Sprintboot16RedisApplication {

    public static void main(String[] args) {
        SpringApplication.run(Sprintboot16RedisApplication.class, args);
    }

}
