package com.example.springboot_21_j2cache.service;

import com.example.springboot_21_j2cache.domain.SMSCode;

public interface SMSCodeService {
    public String sendCodeToSMS(String tele);

    public boolean checkCode(SMSCode smsCode);
}
