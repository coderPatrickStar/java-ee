package com.example.springboot_21_j2cache.domain;

import lombok.Data;

@Data
public class SMSCode {
    private String tele;
    private String code;
}
