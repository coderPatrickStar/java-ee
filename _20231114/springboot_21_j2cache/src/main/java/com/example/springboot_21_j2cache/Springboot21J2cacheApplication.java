package com.example.springboot_21_j2cache;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Springboot21J2cacheApplication {

    public static void main(String[] args) {
        SpringApplication.run(Springboot21J2cacheApplication.class, args);
    }

}
