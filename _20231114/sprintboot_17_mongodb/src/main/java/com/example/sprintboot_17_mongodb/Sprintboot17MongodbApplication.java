package com.example.sprintboot_17_mongodb;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Sprintboot17MongodbApplication {

    public static void main(String[] args) {
        SpringApplication.run(Sprintboot17MongodbApplication.class, args);
    }

}
