package com.itheima;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;

@SpringBootApplication
public class SSMPApplication {

    public static void main(String[] args) {
        ConfigurableApplicationContext run = SpringApplication.run(SSMPApplication.class);
    }

}
