package com.example.springboot_29_bean_properties.bean;

import lombok.Data;

@Data
public class Cat {
    private String name;
    private Integer age;
}
