package com.example.springboot_23_mail.service;

import javax.mail.MessagingException;

public interface SendMailService {
    void sendMail() throws MessagingException;
}
