package com.example.springboot_23_mail;

import com.example.springboot_23_mail.service.SendMailService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import javax.mail.MessagingException;


@SpringBootTest
class Springboot23MailApplicationTests {


    @Autowired
    private SendMailService sendMailService;

    @Test
    void contextLoads() throws MessagingException {
        sendMailService.sendMail();
    }

}
