package cn.hdc.app;

import cn.hdc.bean.Cat;
import cn.hdc.bean.Mouse;
import cn.hdc.config.SpringConfig5;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class App5 {
    public static void main(String[] args) {
        AnnotationConfigApplicationContext ctx = new AnnotationConfigApplicationContext(SpringConfig5.class);
        ctx.registerBean("tom", Cat.class, 0);
        ctx.registerBean("tom", Cat.class, 1);
        ctx.registerBean("tom", Cat.class, 2);
        ctx.register(Mouse.class);
        String[] names = ctx.getBeanDefinitionNames();
        for (String name : names) {
            System.out.println(name);
        }
        System.out.println(ctx.getBean(Cat.class));
    }
}
