package cn.hdc.config;


import cn.hdc.bean.MyImportSelector;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

@Import(MyImportSelector.class)
@Configuration
//@ComponentScan(basePackages = "cn.hdc")
public class SpringConfig6 {

}
