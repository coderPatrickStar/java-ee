package cn.hdc.config;

import cn.hdc.bean.DogFactoryBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;

@ComponentScan({"cn.hdc.bean", "cn,hdc.config"})
public class SpringConfig31 {
//    @Bean
//    public Dog dog() {
//        return new Dog();
//    }

    @Bean
    public DogFactoryBean dog() {
        return new DogFactoryBean();
    }
}
