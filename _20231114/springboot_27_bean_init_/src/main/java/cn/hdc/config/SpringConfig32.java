package cn.hdc.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.ImportResource;
import org.springframework.stereotype.Component;

@ComponentScan({"cn.hdc.bean", "cn,hdc.config"})
@ImportResource("applicationContext1.xml")
@Component
public class SpringConfig32 {
}
