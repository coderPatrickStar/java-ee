package cn.hdc.config;


import cn.hdc.bean.MyPostProcessor;
import cn.hdc.bean.MyRegistar;
import cn.hdc.bean.MyRegistar2;
import cn.hdc.bean.service.impl.BookServiceImpl1;
import org.springframework.context.annotation.Import;

@Import({BookServiceImpl1.class, MyRegistar.class, MyRegistar2.class, MyPostProcessor.class})
public class SpringConfig8 {

}
