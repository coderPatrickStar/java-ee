package com.example;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Sprintboot14TestApplication {

    public static void main(String[] args) {
        SpringApplication.run(Sprintboot14TestApplication.class, args);
    }

}
