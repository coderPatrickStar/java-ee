//package com.example;
//
//import org.junit.jupiter.api.Test;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
//import org.springframework.boot.test.context.SpringBootTest;
//import org.springframework.test.web.servlet.MockMvc;
//import org.springframework.test.web.servlet.ResultActions;
//import org.springframework.test.web.servlet.ResultMatcher;
//import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
//import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
//import org.springframework.test.web.servlet.result.ContentResultMatchers;
//import org.springframework.test.web.servlet.result.HeaderResultMatchers;
//import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
//import org.springframework.test.web.servlet.result.StatusResultMatchers;
//
//@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT)
//@AutoConfigureMockMvc
//public class WebTest {
//    @Test
//    void testWeb(@Autowired MockMvc mvc) throws Exception {
//        MockHttpServletRequestBuilder builder = MockMvcRequestBuilders.get("/books");
//        mvc.perform(builder);
//    }
//
//    @Test
//    void testStatus(@Autowired MockMvc mvc) throws Exception {
//        MockHttpServletRequestBuilder builder = MockMvcRequestBuilders.get("/books");
//        ResultActions action = mvc.perform(builder);
//
//        StatusResultMatchers status = MockMvcResultMatchers.status();
//        ResultMatcher ok = status.isOk();
//
//        action.andExpect(ok);
//    }
//
//    @Test
//    void testBody(@Autowired MockMvc mvc) throws Exception {
//        MockHttpServletRequestBuilder builder = MockMvcRequestBuilders.get("/books");
//        ResultActions action = mvc.perform(builder);
//
//        ContentResultMatchers content = MockMvcResultMatchers.content();
//        ResultMatcher result = content.string("springboot");
//
//        action.andExpect(result);
//    }
//
//    @Test
//    void testJson(@Autowired MockMvc mvc) throws Exception {
//        MockHttpServletRequestBuilder builder = MockMvcRequestBuilders.get("/books");
//        ResultActions action = mvc.perform(builder);
//
//        ContentResultMatchers content = MockMvcResultMatchers.content();
//        ResultMatcher result = content.json("{\"id\":1,\"name\":\"springboot\",\"type\":\"springboot\",\"description\":\"springboot\"}");
//
//        action.andExpect(result);
//    }
//
//    @Test
//    void testContentType(@Autowired MockMvc mvc) throws Exception {
//        MockHttpServletRequestBuilder builder = MockMvcRequestBuilders.get("/books");
//        ResultActions action = mvc.perform(builder);
//
//        HeaderResultMatchers header = MockMvcResultMatchers.header();
//        ResultMatcher contentType = header.string("Content-Type", "application/json");
//        action.andExpect(contentType);
//    }
//
//    @Test
//    void testGetById(@Autowired MockMvc mvc) throws Exception {
//        MockHttpServletRequestBuilder builder = MockMvcRequestBuilders.get("/books");
//        ResultActions action = mvc.perform(builder);
//
//        StatusResultMatchers status = MockMvcResultMatchers.status();
//        ResultMatcher ok = status.isOk();
//        action.andExpect(ok);
//
//        HeaderResultMatchers header = MockMvcResultMatchers.header();
//        ResultMatcher contentType = header.string("Content-Type", "application/json");
//        action.andExpect(contentType);
//
//
//
//        ContentResultMatchers content = MockMvcResultMatchers.content();
//        ResultMatcher result = content.json("{\"id\":1,\"name\":\"springboot\",\"type\":\"springboot\",\"description\":\"springboot\"}");
//        action.andExpect(result);
//    }
//}
