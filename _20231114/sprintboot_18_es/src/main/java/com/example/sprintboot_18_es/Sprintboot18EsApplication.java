package com.example.sprintboot_18_es;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Sprintboot18EsApplication {

    public static void main(String[] args) {
        SpringApplication.run(Sprintboot18EsApplication.class, args);
    }

}
