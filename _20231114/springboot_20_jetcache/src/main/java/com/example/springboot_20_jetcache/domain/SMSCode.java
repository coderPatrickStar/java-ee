package com.example.springboot_20_jetcache.domain;

import lombok.Data;

@Data
public class SMSCode {
    private String tele;
    private String code;
}
