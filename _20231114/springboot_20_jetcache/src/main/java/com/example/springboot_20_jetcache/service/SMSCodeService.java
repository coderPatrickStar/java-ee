package com.example.springboot_20_jetcache.service;

import com.example.springboot_20_jetcache.domain.SMSCode;

public interface SMSCodeService {
    public String sendCodeToSMS(String tele);

    public boolean checkCode(SMSCode smsCode);
}
