package com.example.sprintboot_19_cache.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.example.sprintboot_19_cache.domain.Book;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface BookDao extends BaseMapper<Book> {
}
