package com.example.sprintboot_19_cache.service.impl;

import com.example.sprintboot_19_cache.dao.BookDao;
import com.example.sprintboot_19_cache.domain.Book;
import com.example.sprintboot_19_cache.service.BookService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;

@Service
public class BookServiceImpl implements BookService {
    @Autowired
    private BookDao bookDao;


//    private HashMap<Integer, Book> cache = new HashMap<Integer, Book>();
//
//    @Override
//    public Book getById(Integer id) {
//        Book book = cache.get(id);
//        if (book == null) {
//            Book queryBook = bookDao.selectById(id);
//            cache.put(id, queryBook);
//            return queryBook;
//        }
//        return cache.get(id);
//    }

    @Override
    @Cacheable(value = "cacheSpace", key = "#id")
    public Book getById(Integer id) {
        Book queryBook = bookDao.selectById(id);
        return queryBook;
    }


    @Override
    public boolean save(Book book) {
        return bookDao.insert(book) > 0;
    }

    @Override
    public boolean update(Book book) {
        return bookDao.updateById(book) > 0;
    }

    @Override
    public boolean delete(Integer id) {
        return bookDao.deleteById(id) > 0;
    }

    @Override
    public List<Book> getAll() {
        return bookDao.selectList(null);
    }
}
