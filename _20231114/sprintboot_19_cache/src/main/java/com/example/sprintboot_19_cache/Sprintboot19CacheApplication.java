package com.example.sprintboot_19_cache;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;

@SpringBootApplication
@EnableCaching
public class Sprintboot19CacheApplication {

    public static void main(String[] args) {
        SpringApplication.run(Sprintboot19CacheApplication.class, args);
    }

}
