package com.example.sprintboot_19_cache.domain;

import lombok.Data;

@Data
public class SMSCode {
    private String tele;
    private String code;
}
