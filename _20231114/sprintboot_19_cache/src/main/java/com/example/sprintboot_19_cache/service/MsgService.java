package com.example.sprintboot_19_cache.service;

public interface MsgService {
    public String get(String tele);

    public boolean check(String tele, String code);
}
