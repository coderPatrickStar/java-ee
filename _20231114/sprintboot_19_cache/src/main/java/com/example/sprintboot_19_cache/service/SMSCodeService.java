package com.example.sprintboot_19_cache.service;

import com.example.sprintboot_19_cache.domain.SMSCode;

public interface SMSCodeService {
    public String sendCodeToSMS(String tele);

    public boolean checkCode(SMSCode smsCode);
}
