package cn.hdc.config;

import cn.hdc.bean.Cat;
import cn.hdc.bean.Dog;
import cn.hdc.bean.Mouse;
import cn.hdc.bean.MyImportSelector;
import com.alibaba.druid.pool.DruidDataSource;
import org.springframework.boot.autoconfigure.condition.*;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Conditional;
import org.springframework.context.annotation.Import;

//@Import(MyImportSelector.class)
//@Import(Mouse.class)
@ComponentScan("cn.hdc.bean")
public class SpringConfig {
//    @Bean
////    @ConditionalOnClass(name = "cn.hdc.bean.Mouse")
////    @ConditionalOnMissingClass("cn.hdc.bean.Wolf")
//    @ConditionalOnBean(name = "jerry")
////    @ConditionalOnMissingClass("cn.hdc.bean.Dog")
//    @ConditionalOnNotWebApplication
////    @ConditionalOnWebApplication
//    public Cat tom() {
//        return new Cat();
//    }

    @Bean
    @ConditionalOnClass(name = "com.mysql.jdbc.Driver")
    public DruidDataSource dataSource() {
        return new DruidDataSource();
    }
}
