package cn.hdc.mapper;

import cn.hdc.pojo.Student;
import cn.hdc.utils.MybaitsUtils;
import org.apache.ibatis.session.SqlSession;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;


public class StudentMapperTest {

    private SqlSession session;

    @Before
    public void setUp() throws Exception {
        session = MybaitsUtils.getSession();
    }

    @After
    public void tearDown() throws Exception {
        session.commit();
        session.close();
    }

    @Test
    public void findStudentByNameAndMajor() {
        Student student = new Student();
        student.setName("张三");
        student.setMajor("英语");
        List<Student> students = session.selectList("cn.hdc.mapper.StudentMapper.findStudentByNameAndMajor", student);
        for (Student stu : students) {
            System.out.println(stu);
        }
    }

    @Test
    public void findStudentByList() {
        List<Integer> list = new ArrayList<>();
        for (int i = 0; i < 5; i++) {
            list.add(i);
        }
        List<Student> students = session.selectList("cn.hdc.mapper.StudentMapper.findStudentByList", list);
        for (Student stu : students) {
            System.out.println(stu);
        }
    }
}