package cn.hdc.mapper;

import cn.hdc.pojo.Student;

import java.util.List;

public interface StudentMapper {
    public Student findStudentByNameAndMajor(Student student);

    public Student findStudentByList(List<Integer> list);
}
