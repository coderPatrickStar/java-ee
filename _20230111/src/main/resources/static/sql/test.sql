-- 创建数据库
create database if not exists 'test20230105';
-- 使⽤数据库
use test20230105;
-- 创建表
create table if not exists 'soft_bookrack'
(
    'book_name'   varchar(32) not null,
    'book_author' varchar(32) not null,
    'book_isbn'   varchar(32) not null primary key
);