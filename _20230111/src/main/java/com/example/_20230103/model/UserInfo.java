package com.example._20230103.model;

import lombok.Data;

import java.time.LocalDateTime;
import java.util.List;

@Data
public class UserInfo {
    private int id;
    private String username;
    private String password;
    private String photo;
    private LocalDateTime createtime;
    private LocalDateTime updatetime;
    private int state;
    private List<ArticleInfo> artlist;

    public UserInfo() {

    }

    public UserInfo(int id, String username) {
        this.id = id;
        this.username = username;
    }
}
