package com.example._20230103.service;

import com.example._20230103.mapper.UserMapper;
import com.example._20230103.model.UserInfo;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

@Service
public class UserService {
    @Resource
    private UserMapper userMapper;

    public int addUser(String username, String password) {
        Integer flag = userMapper.addUser(username, password);
        return flag;
    }

    public UserInfo getUserById(Integer id) {
        return userMapper.getUserById(id);
    }
}
