package com.example._20230103.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

@RestController
@RequestMapping("/user")
public class UserController2 {
    @RequestMapping("/m1")
    public Object method(HttpServletRequest request) {
        HttpSession session = request.getSession(false);
        if (session != null && session.getAttribute("userinfo") != null) {
            //说明已经登录，业务处理
            return true;
        } else {
            //未登录
            return false;
        }
    }

    @RequestMapping("/m2")
    public Object method2(HttpServletRequest request) {
        //有session就获取，没有不会创建
        HttpSession session = request.getSession(false);
        if (session != null && session.getAttribute("userinfo") != null) {
            //说明已经登录，业务处理
            return true;
        } else {
            //未登录
            return false;
        }
    }
}
