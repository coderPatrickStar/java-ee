package com.example._20230103.controller;

import com.example._20230103.model.User;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.HashMap;

@Controller
@RequestMapping("/test")
public class TestController {
    @RequestMapping("/index")
    public String getIndex() {
        return "index.html";
    }

    @GetMapping("/calc")
    @ResponseBody
    public String calc(Integer num1, Integer num2) {
        return String.format("<h1>计算结果： %d</h1>", (num1 + num2));
    }

    @GetMapping("/json")
    @ResponseBody
    public HashMap<String, String> json() {
        HashMap<String, String> map = new HashMap<>();
        map.put("name", "tanlei");
        map.put("password", "666666");
        return map;
    }

    @GetMapping("/json2")
    @ResponseBody
    public String json2() {
        return "{\"password\":\"666666\",\"name\":\"tanlei\"}";
    }


    @RequestMapping("/login")
    @ResponseBody
    public HashMap<String, Object> login(@RequestBody User user) {
        HashMap<String, Object> map = new HashMap<>();
        int succ = 200;
        int result = 0;
        String msg = "用户名或密码错误！";
        if (StringUtils.hasLength(user.getUsername()) && StringUtils.hasLength(user.getPassword())
                && user.getUsername().equals("admin") && user.getPassword().equals("admin")) {
            result = 1;
            msg = "";
        } else {
            result = 0;
        }
        map.put("succ", succ);
        map.put("result", result);
        map.put("msg", msg);
        return map;
    }

    //请求重定向
    @RequestMapping("/index1")
    public String index_1() {
        return "redirect:/index.html";
    }

    //请求转发
    @RequestMapping("/index2")
    public String index_2() {
        return "forward:/index.html";
    }
}
