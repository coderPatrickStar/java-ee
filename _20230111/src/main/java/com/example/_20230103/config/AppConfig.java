//package com.example._20230103.config;
//
//import org.springframework.context.annotation.Configuration;
//import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
//import org.springframework.web.servlet.config.annotation.PathMatchConfigurer;
//import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
//
////全局配置文件
//@Configuration
//public class AppConfig implements WebMvcConfigurer {
//    //添加拦截器，制定拦截规则
//    @Override
//    public void addInterceptors(InterceptorRegistry registry) {
//        registry.addInterceptor(new LoginInterceptor())
//                .addPathPatterns("/**")                 //拦截规则：拦截所有的url
//                .excludePathPatterns("/user/login");    //配置不拦截的url
//    }
//}
