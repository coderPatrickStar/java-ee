package com.example._20230103.config;

import org.springframework.core.MethodParameter;
import org.springframework.http.MediaType;
import org.springframework.http.server.ServerHttpRequest;
import org.springframework.http.server.ServerHttpResponse;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.servlet.mvc.method.annotation.ResponseBodyAdvice;

import java.util.HashMap;

@ControllerAdvice
public class MyResponseBodyAdvice implements ResponseBodyAdvice {
    //是否需要在返回数据之前重写数据
    @Override
    public boolean supports(MethodParameter returnType, Class converterType) {
        return true;
    }

    //最终返回给前端封装的数据对象
    @Override
    public Object beforeBodyWrite(Object body, MethodParameter returnType, MediaType selectedContentType, Class selectedConverterType, ServerHttpRequest request, ServerHttpResponse response) {
        HashMap<String, Object> map = new HashMap<>();
        map.put("succ", 200);
        map.put("data", body);
        map.put("msg", "");
        return map;
    }
}
