package com.example._20230103.service;

import com.example._20230103.mapper.UserMapper;
import com.example._20230103.model.UserInfo;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;

@Service
public class UserService {
    @Resource
    private UserMapper userMapper;

    @Transactional(propagation = Propagation.REQUIRED)
    public int addUser(String username, String password) {
        int result = userMapper.addUser(username, password);
        System.out.println("添加用户：" + result);
        return result;
    }

    public UserInfo getUserById(Integer id) {
        return userMapper.getUserById(id);
    }
}
