package com.example._20230103.commons;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.stereotype.Component;

@Aspect
@Component
public class UserAspect2 {
    //定义切点方法 controller 包下，子孙包下所有类的所有方法
    @Pointcut("execution(* com.example._20230103.controller..*.*(..))")
    public void pointcut() {

    }

    //前置方法
    @Before("pointcut()")
    public void doBefore() {

    }

    //环绕方法
    @Around("pointcut()")
    public Object doAround(ProceedingJoinPoint joinPoint) {
        Object obj = null;
        System.out.println("Around 方法开始执行");
        try {
            obj = joinPoint.proceed();
        } catch (Throwable e) {
            throw new RuntimeException(e);
        }
        System.out.println("Around 方法结束执行");
        return obj;
    }
}
