package com.example._20230103.commons;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.*;
import org.springframework.stereotype.Component;

@Aspect     //表示当前的类为切面
@Component
public class UserAspect {
    //定义切点，这里使用AspectJ表达式语法
    @Pointcut("execution(* com.example._20230103.controller.UserController.*(..))")
    public void pointcut() {

    }

    //前置通知
    @Before("pointcut()")
    public void before() {
        System.out.println("执行了前置方法！");
    }

    //后置通知
    @After("pointcut()")
    public void after() {
        System.out.println("执行了后置方法！");
    }
    // return 之前通知
    @AfterReturning("pointcut()")
    public void afterReturning() {
        System.out.println("执行了AfterReturning方法！");
    }
    // 抛出异常之前通知
    @AfterThrowing("pointcut()")
    public void afterThrowing() {
        System.out.println("执行了AfterThrowing方法！");
    }

    //添加环绕通知
    @Around("pointcut()")
    public Object around(ProceedingJoinPoint joinPoint) throws Throwable {
        Object obj = null;
        System.out.println("执行环绕通知的前置方法");
        obj = joinPoint.proceed(); //执行拦截的方法
        System.out.println("执行环绕通知的后置方法");
        return obj;
    }
}
