//package com.example._20230103.javase;
//
//import java.lang.reflect.InvocationHandler;
//import java.lang.reflect.Method;
//import java.lang.reflect.Proxy;
//
////动态代理：使用JDK提供的api(InvocationHandler/Proxy实现)，此种方式实现，要求被代理类必须实现接口
//public class PayServiceJDKInvocationHandler implements InvocationHandler {
//    //目标对象即就是被代理对象
//    private Object target;
//
//    public PayServiceJDKInvocationHandler(Object target) {
//        this.target = target;
//    }
//
//    //proxy代理对象
//    @Override
//    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
//        //1.安全检查
//        System.out.println("安全检查");
//        //2.记录日志
//        System.out.println("记录日志");
//        //3.时间统计开始
//        System.out.println("记录开始时间");
//
//        //通过反射调用被代理类的方法
//        Object retVal = method.invoke(target, args);
//
//        //时间统计结束
//        System.out.println("记录结束时间");
//        return retVal;
//    }
//
//    public static void main(String[] args) {
//        PayService target = new AliPayService();
//        //方法调用处理器
//        InvocationHandler handler = new PayServiceJDKInvocationHandler(target);
//        //创建一个代理类：通过被代理类、被代理类实现的接口、方法调用处理器来创建
//        PayService proxy = (PayService) Proxy.newProxyInstance(
//                target.getClass().getClassLoader(),
//                new Class[]{PayService.class},
//                handler
//        );
//        proxy.pay();
//    }
//}
