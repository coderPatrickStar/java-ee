package com.baidu.beans;

import org.springframework.stereotype.Service;

@Service
public class UserService {
    public void sayHi(String name) {
        System.out.println("hello " + name);
    }
}
