import com.baidu.beans.*;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.xml.XmlBeanFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;

public class App {
    public static void main(String[] args) {
//        ApplicationContext applicationContext =
//            new ClassPathXmlApplicationContext("spring-config.xml");
//        User user = (User) applicationContext.getBean("user");
//        user.sayHi();

//        BeanFactory beanFactory = new XmlBeanFactory(new ClassPathResource("spring-config.xml"));

        //BeanFactory beanFactory1 = new ClassPathXmlApplicationContext("spring-config.xml");
//        User user = (User) beanFactory.getBean("user");
//        user.sayHi();
//
//        User user1 = (User) beanFactory.getBean("user1");
//        user1.sayHi();
//
//        System.out.println(user1);
//        System.out.println(user);

//        User user = beanFactory.getBean(User.class);
//        user.sayHi();
//
//        User user = beanFactory.getBean("user",User.class);
//        user.sayHi();

        ApplicationContext context = new ClassPathXmlApplicationContext("spring-config.xml");
//        UserController userController = context.getBean("userController",UserController.class);
//        userController.sayHi();

//        UserService userService = context.getBean("userService", UserService.class);
//        userService.sayHi("lihua");
//        UserRepository userRepository = context.getBean("userRepository",UserRepository.class);
//        userRepository.sayHi();
//
//        UserCompont userCompont = context.getBean("userCompont",UserCompont.class);
//        userCompont.sayHi();

//        UserConfiguration userConfiguration = context.getBean("userConfiguration",UserConfiguration.class);
//        userConfiguration.showConfig();
        UService uService = context.getBean("UService",UService.class);
        uService.sayHi();
    }
}
