package com.example._20230103.mapper;

import com.example._20230103.model.UserInfo;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface UserMapper {
    public UserInfo getUserById(Integer id);
}
