package demo2;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class Master {

    private Cat cat;

    private Dog dog;

    @Autowired
    public Master(Cat cat, Dog dog) {
        this.cat = cat;
        this.dog = dog;
    }

    @Override
    public String toString() {
        return "Master{" +
                "cat=" + cat +
                ", dog=" + dog +
                '}';
    }
}
