package com.example._20230103.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.HashMap;

@Controller
public class TestController {
    @RequestMapping("/index")
    public String getIndex() {
        return "index.html";
    }

    @GetMapping("/calc")
    @ResponseBody
    public String calc(Integer num1, Integer num2) {
        return String.format("<h1>计算结果： %d</h1>", (num1 + num2));
    }

    @GetMapping("/json")
    @ResponseBody
    public HashMap<String, String> json() {
        HashMap<String, String> map = new HashMap<>();
        map.put("name", "tanlei");
        map.put("password", "666666");
        return map;
    }

    @GetMapping("/json2")
    @ResponseBody
    public String json2() {
        return "{\"password\":\"666666\",\"name\":\"tanlei\"}";
    }
}
