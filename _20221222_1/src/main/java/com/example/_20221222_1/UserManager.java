package com.example._20221222_1;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class UserManager {
    @RequestMapping("/userlist")
    public String userlist() {
        return "<h1>查询用户成功<h1>";
    }

    @RequestMapping("/adduser")
    public String adduser() {
        return "<h1>添加用户成功<h1>";
    }
}
