package cn.hdc.mapper;

import cn.hdc.pojo.Student;
import cn.hdc.utils.MybatisUtils;
import org.apache.ibatis.session.SqlSession;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

public class StudentMapperTest {

    private SqlSession session;

    @Before
    public void setUp() throws Exception {
        session = MybatisUtils.getSession();
    }

    @After
    public void tearDown() throws Exception {
        session.close();
    }

    @Test
    public void findStudentByNameAndMajor() {
        Student student = new Student();
        List<Student> students = session.selectList("cn.hdc.mapper.StudentMapper.findStudentByNameAndMajor", student);
        for (Student student1 : students) {
            System.out.println(student1);
        }
    }

    @Test
    public void findByList() {
        List<Integer> ids = new ArrayList<>();
        for (int i = 1; i < 5; i++) {
            ids.add(i);
        }
        List<Student> students = session.selectList("cn.hdc.mapper.StudentMapper.findByList", ids);
        for (Student student : students) {
            System.out.println(student);
        }
    }
}