package com.example._20230103.controller;

import com.example._20230103.model.UserInfo;
import com.example._20230103.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class UserController1 {
    @Autowired
    private UserService userService;

    @RequestMapping("/getuser")
    public UserInfo getUserById(Integer id) {
        UserInfo userInfo = null;
        if (id != null && id > 0) {
            userInfo = userService.getUserById(id);
        }
        return userInfo;
    }
}
