package com.example._20230103.mapper;

import com.example._20230103.model.UserInfo;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface UserMapper {
    //根据用户id，查询用户的信息
    public UserInfo getUserById(Integer id);

    //添加用户数据
    public int addUser(String username, String password);

    //添加方法 (得到添加成功的主键id)
    public int addUser2(UserInfo userInfo);

    //修改id为1的username=abc password=abc
    public int updateUser(UserInfo userInfo);

    //删除id为1的用户
    public int deleteUser(UserInfo userInfo);
}
