package com.example._20230103.mapper;

import com.example._20230103.model.UserInfo;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
class UserMapperTest {
    @Autowired
    private UserMapper userMapper;

    @Test
    void getUserById() {
        System.out.println(userMapper.getUserById(1));
    }

    @Test
    void addUser() {
        int flag = userMapper.addUser("tanlei", "666");
        System.out.println("受影响的行数 " + flag);
    }

    @Test
    void addUser2() {
        UserInfo userInfo = new UserInfo();
        userInfo.setUsername("Mybaits");
        userInfo.setPassword("4564");
        userMapper.addUser2(userInfo);
        System.out.println("自增id " + userInfo.getId());
    }

    @Test
    void updateUser() {
        UserInfo userInfo = new UserInfo();
        userInfo.setUsername("abc");
        userInfo.setPassword("abc");
        userInfo.setId(1);
        int flag = userMapper.updateUser(userInfo);
        System.out.println("受影响的行数： " + flag);
    }

    @Test
    void deleteUser() {
        UserInfo userInfo = new UserInfo();
        userInfo.setId(1);
        int flag = userMapper.deleteUser(userInfo);
        System.out.println("受影响的行数： " + flag);
    }
}