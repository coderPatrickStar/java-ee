package cn.hdc.mapper;

import cn.hdc.pojo.Category;

public interface CategoryMapper {
    public Category findCategoryWithProduct(Integer id);
}
