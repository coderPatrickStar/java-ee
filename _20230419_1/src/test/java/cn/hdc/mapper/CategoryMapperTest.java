package cn.hdc.mapper;

import cn.hdc.pojo.Category;
import cn.hdc.utils.MybaitsUtils;
import org.apache.ibatis.session.SqlSession;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class CategoryMapperTest {
    private SqlSession session;

    @Before
    public void setUp() throws Exception {
        this.session = MybaitsUtils.getSession();
    }

    @After
    public void tearDown() throws Exception {
        this.session.commit();
        this.session.close();
    }

    @Test
    public void findCategoryWithProduct() {
        Category category = session.selectOne("cn.hdc.mapper.CategoryMapper.findCategoryWithProduct", 2);
        System.out.println(category);
    }
}