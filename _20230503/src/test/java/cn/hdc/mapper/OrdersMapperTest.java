package cn.hdc.mapper;

import cn.hdc.pojo.Orders;
import cn.hdc.utils.MybatisUtils;
import org.apache.ibatis.session.SqlSession;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class OrdersMapperTest {
    private SqlSession session;
    private OrdersMapper ordersMapper;

    @Before
    public void setUp() throws Exception {
        session = MybatisUtils.getSession();
    }

    @After
    public void tearDown() throws Exception {
        session.commit();
        session.close();
    }

    @Test
    public void selectOrderById() {
        ordersMapper = session.getMapper(OrdersMapper.class);
        Orders orders = ordersMapper.selectOrderById(1);
        System.out.println(orders);
    }
}