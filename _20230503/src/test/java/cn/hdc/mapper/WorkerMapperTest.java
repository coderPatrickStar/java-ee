package cn.hdc.mapper;

import cn.hdc.pojo.Worker;
import cn.hdc.utils.MybatisUtils;
import org.apache.ibatis.session.SqlSession;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class WorkerMapperTest {
    private WorkerMapper workerMapper;
    private SqlSession session;

    @Before
    public void setUp() throws Exception {
        session = MybatisUtils.getSession();
    }

    @After
    public void tearDown() throws Exception {
        session.commit();
        session.close();
    }

    @Test
    public void findWorker() {
        workerMapper = session.getMapper(WorkerMapper.class);
        Worker worker = workerMapper.findWorker(1);
        System.out.println(worker);
    }

    @Test
    public void insertWorker() {
        workerMapper = session.getMapper(WorkerMapper.class);
        Worker worker = new Worker();
        worker.setName("赵六");
        worker.setAge(23);
        worker.setSex("男");
        worker.setWorker_id("1004");
        int i = workerMapper.insertWorker(worker);
        if (i > 0) {
            System.out.println("插入成功!");
        } else {
            System.out.println("插入失败！");
        }
    }

    @Test
    public void updateWorker() {
        workerMapper = session.getMapper(WorkerMapper.class);
        Worker worker = new Worker();
        worker.setName("李华");
        worker.setAge(30);
        worker.setId(4);
        int ret = workerMapper.updateWorker(worker);
        if (ret > 0) {
            System.out.println("修改成功!");
        } else {
            System.out.println("修改失败！");
        }
    }

    @Test
    public void deleteWorker() {
        workerMapper = session.getMapper(WorkerMapper.class);
        int ret = workerMapper.deleteWorker(4);
        if (ret > 0) {
            System.out.println("删除成功!");
        } else {
            System.out.println("删除失败！");
        }
    }

    @Test
    public void selectWorkerByIdAndName() {
        workerMapper = session.getMapper(WorkerMapper.class);
        Worker worker = workerMapper.selectWorkerByIdAndName(3, "王五");
        System.out.println(worker);
    }
}