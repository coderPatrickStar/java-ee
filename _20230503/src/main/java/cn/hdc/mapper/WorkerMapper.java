package cn.hdc.mapper;

import cn.hdc.pojo.Worker;
import org.apache.ibatis.annotations.*;

public interface WorkerMapper {
    @Select("select * from tb_worker where id=#{id}")
    public Worker findWorker(int id);

    @Insert("insert into tb_worker values (null,#{name},#{age},#{sex},#{worker_id})")
    public int insertWorker(Worker worker);

    @Update("update tb_worker set name=#{name},age=#{age} where id=#{id}")
    public int updateWorker(Worker worker);

    @Delete("delete from tb_worker where id=#{id}")
    public int deleteWorker(int id);


    @Select("select * from tb_worker where id=#{param1} and name=#{param2}")
    public Worker selectWorkerByIdAndName(@Param("param1") int id, @Param("param2") String name);

}
