package cn.hdc.mapper;

import cn.hdc.pojo.Orders;
import org.apache.ibatis.annotations.Many;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.Select;

import java.util.List;

public interface OrdersMapper {
//    @Select("select * from tb_orders where user_id = #{uid}")
//    @Results({
//            @Result(id = true, property = "id", column = "id"),
//            @Result(property = "number", column = "number"),
//            @Result(property = "userId", column = "user_id")
//    })
//    public List<Orders> selectOrdersByUserId(int uid);

    @Select("select * from tb_orders where id = #{id}")
    @Results({
            @Result(id = true, column = "id", property = "id"),
            @Result(column = "number", property = "number"),
            @Result(column = "id", property = "productList", many = @Many(select = "cn.hdc.mapper.ProductMapper.selectProductsByOrdersId"))
    })
    public Orders selectOrderById(int id);
}
