package cn.hdc.mapper;

import cn.hdc.pojo.Person;
import org.apache.ibatis.annotations.One;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.Select;

public interface PersonMapper {
    @Select("select * from tb_person where id=#{id}")
    @Results(@Result(
            column = "card_id",
            property = "idCard",
            one = @One(select = "cn.hdc.mapper.IdCardMapper.selectIdCardById")
    ))
    public Person selectPersonById(int id);
}
