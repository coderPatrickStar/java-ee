package cn.hdc.mapper;

import cn.hdc.pojo.User;
import org.apache.ibatis.annotations.*;

public interface UserMapper {
    @Select("select * from tb_user where id = #{id}")
    @Results({
            @Result(id = true,property = "id",column = "id"),
            @Result(property = "username",column = "username"),
            @Result(property = "address",column = "address"),
            @Result(property = "ordersList",column = "id",many = @Many(select = "cn.hdc.mapper.OrdersMapper.selectOrdersByUserId"))
    })
    public User selectUserById(int id);
}
