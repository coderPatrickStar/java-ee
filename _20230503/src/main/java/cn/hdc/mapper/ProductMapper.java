package cn.hdc.mapper;

import cn.hdc.pojo.Product;
import org.apache.ibatis.annotations.Select;

import java.util.List;

public interface ProductMapper {
    @Select("select * from tb_product where id in (select product_id from tb_ordersitem where orders_id=#{orderId})")
    public List<Product> selectProductsByOrdersId(int orderId);
}
