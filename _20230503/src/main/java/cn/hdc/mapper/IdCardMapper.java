package cn.hdc.mapper;

import cn.hdc.pojo.IdCard;
import org.apache.ibatis.annotations.Select;

public interface IdCardMapper {
    @Select("select * from tb_idCard where id = #{id}")
    public IdCard selectIdCardById(int id);
}
