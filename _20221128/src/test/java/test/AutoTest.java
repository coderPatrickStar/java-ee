package test;

import org.openqa.selenium.By;
import org.openqa.selenium.chrome.ChromeDriver;

public class AutoTest {
    public void test() throws InterruptedException {
        ChromeDriver driver = new ChromeDriver();
        Thread.sleep(1000);
        driver.get("https://www.baidu.com");
        System.out.println("输入百度成功");
        Thread.sleep(1000);
        driver.findElement(By.cssSelector("#kw")).sendKeys("石家庄");
        System.out.println("输入石家庄成功");
        Thread.sleep(1000);
        driver.findElement(By.cssSelector("#su")).click();
        System.out.println("点击搜索成功");
        Thread.sleep(5000);
        driver.quit();
    }

}
