import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

public class Parser {
    private static final String INPUT_PATH = "C:/Users/Administrator/Desktop/test/jdk-8u341-docs-all/docs/api/";

    public void run() {
        //1
        ArrayList<File> filelist = new ArrayList<>();
        enumFile(INPUT_PATH, filelist);
//        System.out.println(filelist);
//        System.out.println(filelist.size());

        for (File f : filelist) {
            parseHtml(f);
        }
    }

    private void parseHtml(File f) {
        String title = parseTitle(f);
        String url = parseUrl(f);
        String content = parseContent(f);
    }

    public String parseContent(File f) {
        try (FileReader reader = new FileReader(f)) {
            boolean isCopy = true;
            StringBuilder content = new StringBuilder();

            while (true) {
                int ret = reader.read();
                if (ret == -1) {
                    break;
                }

                char c = (char) ret;

                if (isCopy) {
                    if (c == '<') {
                        isCopy = false;
                        continue;
                    }

                    if (c == '\n' || c == '\r') {
                        c = ' ';
                    }

                    content.append(c);
                } else {
                    if (c == '>') {
                        isCopy = true;
                    }
                }
            }

            return content.toString();

        } catch (IOException e) {
            e.printStackTrace();
        }
        return "";
    }

    private String parseUrl(File f) {
        String part1 = "https://docs.oracle.com/javase/8/docs/api/";
        String part2 = f.getAbsolutePath().substring(INPUT_PATH.length());
        String result = part1 + part2;
        return result;
    }

    private String parseTitle(File f) {
        String name = f.getName();
        return name.substring(0, f.getName().length() - ".html".length());
    }

    private void enumFile(String inputPath, ArrayList<File> filelist) {
        File rootPath = new File(inputPath);
        File[] files = rootPath.listFiles();

        for (File f : files) {
            if (f.isDirectory()) {
                enumFile(f.getAbsolutePath(), filelist);
            } else {
                if (f.getAbsolutePath().endsWith(".html")) {
                    filelist.add(f);
                }
            }
        }
    }

    public static void main(String[] args) {
        Parser parser = new Parser();
        parser.run();
    }
}
