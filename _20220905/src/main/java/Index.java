import org.ansj.domain.Term;
import org.ansj.splitWord.analysis.ToAnalysis;

import java.sql.Wrapper;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class Index {
    private ArrayList<DocInfo> forwardIndex = new ArrayList<>();

    private HashMap<String,ArrayList<Weight>> invertedIndex = new HashMap<>();

    public DocInfo getDocInfo(int docId){
        return forwardIndex.get(docId);
    }

    public List<Weight> getInverted(String term){
        return invertedIndex.get(term);
    }

    public void addDoc(String title,String url,String content){
       DocInfo docInfo = buildForward(title,url,content);
       buildInverted(docInfo);
    }

    private void buildInverted(DocInfo docInfo) {
        class WordCnt{
            public int titlecount;
            public int contentcount;
        }

        HashMap<String,WordCnt> wordCntHashMap = new HashMap<>();

        List<Term> terms = ToAnalysis.parse(docInfo.getTitle()).getTerms();

        for (Term term : terms){
            String word = term.getName();
            WordCnt wordCnt = wordCntHashMap.get(word);
            if(wordCnt == null){
                WordCnt newWordCnt = new WordCnt();
                newWordCnt.titlecount = 1;
                newWordCnt.contentcount = 0;
                wordCntHashMap.put(word,newWordCnt);
            }else {
                wordCnt.titlecount += 1;
            }
        }

    }

    private DocInfo buildForward(String title, String url, String content) {
        DocInfo docInfo = new DocInfo();
        docInfo.setDocId(forwardIndex.size());
        docInfo.setTitle(title);
        docInfo.setUrl(url);
        docInfo.setContent(content);
        forwardIndex.add(docInfo);
    }

    public void save(){

    }

    public void load(){

    }
}
