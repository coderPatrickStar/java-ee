import org.ansj.domain.Term;
import org.ansj.splitWord.analysis.ToAnalysis;

import java.io.File;
import java.util.List;

public class TestAnsj {
    private static final String str = "C:\\Users\\Administrator\\Desktop\\test\\jdk-8u341-docs-all\\docs\\api\\java\\sql\\Time.html";
    public static void main(String[] args) {
        String str = "i have a dream";
        List<Term> terms = ToAnalysis.parse(str).getTerms();

        for (Term term : terms){
            System.out.println(term);
        }
    }
}
