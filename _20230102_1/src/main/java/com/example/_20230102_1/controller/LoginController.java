package com.example._20230102_1.controller;

import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;

@Controller
@ResponseBody
@Slf4j
public class LoginController {
    //得到日志对象
    private Logger logger = LoggerFactory.getLogger(LoginController.class);
    //

    @RequestMapping("/login")
//    public String login(HttpServletRequest req) {
    public String login(String username, String password) {
        logger.trace("日志级别 trace");
        logger.info(username + " 登录，日志级别 INFO ");
        logger.debug("日志级别 debug");
        logger.warn("日志级别 warn");
        logger.error("日志级别 error");
        String result = "未知错误";
//        String username = req.getParameter("username");
//        String password = req.getParameter("password");
//        if (username != null && !username.equals("") && password != null && !password.equals("")) {
//            //
//            if (username.equals("admin") && password.equals("admin")) {
//                return "登录成功！";
//            } else {
//                return "用户名或密码错误！";
//            }
//        } else {
//            result = "非法参数";
//        }
        if (StringUtils.hasLength(username) && StringUtils.hasLength(password)) {
            if (username.equals("admin") && password.equals("admin")) {
                return "登录成功！";
            } else {
                return "登录失败！用户名或密码错误！";
            }
        }
        return result;
    }
}
