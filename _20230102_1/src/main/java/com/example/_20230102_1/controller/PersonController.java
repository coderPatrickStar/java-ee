package com.example._20230102_1.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/per")
@Slf4j
public class PersonController {
    @RequestMapping("/log")
    public void loggerTest() {
        log.error("---------------error---------------");
    }
}
