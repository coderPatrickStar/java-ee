package cn.hdc.controller.test;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class ParasController {
    @RequestMapping(value = "/params", params = "id=1")
    public String findById(String id) {
        System.out.println("findByID" + id);
        return "success";
    }
}
