package cn.hdc.controller.test;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/ant")
public class AntPathController {
    @RequestMapping("/ant1?")
    public String ant1() {
        System.out.println("ant1方法");
        return "success";
    }

    @RequestMapping("/ant2/*.do")
    public String ant2() {
        System.out.println("ant2方法");
        return "success";
    }

    @RequestMapping("/*/ant3")
    public String ant3() {
        System.out.println("ant3方法");
        return "success";
    }

    @RequestMapping("/**/ant4")
    public String ant4() {
        System.out.println("ant4方法");
        return "success";
    }

    @RequestMapping("/ant5/**")
    public String ant5() {
        System.out.println("ant5方法");
        return "success";
    }

}
