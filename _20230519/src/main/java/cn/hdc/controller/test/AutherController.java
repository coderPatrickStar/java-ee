package cn.hdc.controller.test;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class AutherController {
    @RequestMapping(value = {"/adduser", "/deleteuser"})
    public String checkAuth() {
        System.out.println("增删改检验");
        return "success";
    }
}
