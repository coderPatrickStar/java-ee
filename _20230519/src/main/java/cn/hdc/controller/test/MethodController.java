package cn.hdc.controller.test;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
@RequestMapping("/method")
public class MethodController {
    @RequestMapping(method = RequestMethod.GET)
    public String get() {
        System.out.println("RequestMethod.get");
        return "sucess";
    }

    @RequestMapping(method = RequestMethod.POST)
    public String post() {
        System.out.println("RequestMethod.post");
        return "sucess";
    }

    @RequestMapping(method = RequestMethod.DELETE)
    public String delete() {
        System.out.println("RequestMethod.delete");
        return "sucess";
    }

    @RequestMapping(method = RequestMethod.PUT)
    public String put() {
        System.out.println("RequestMethod.put");
        return "sucess";
    }
}
