package cn.hdc.controller.test;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

@Controller
public class UserController {
    @GetMapping("/{id}")
    public String findById(@PathVariable("id") String id) {
        System.out.println("根据Id查询" + id);
        return "success";
    }

    @DeleteMapping("/{id}")
    public String deleteById(@PathVariable("id") String id) {
        System.out.println("根据Id删除" + id);
        return "success";
    }

    @PutMapping("/{id}")
    public String updateById(@PathVariable("id") String id) {
        System.out.println("根据Id更新" + id);
        return "success";
    }

    @PostMapping("/{id}")
    public String add(@PathVariable("id") String id) {
        System.out.println("新增" + id);
        return "success";
    }

}
