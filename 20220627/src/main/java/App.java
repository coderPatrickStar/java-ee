import com.hdc.beans.User;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.xml.XmlBeanFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.context.support.GenericApplicationContext;
import org.springframework.core.io.ClassPathResource;

/**
 * Creat by TanLei
 * Description:
 * User:86183
 * Date:2022-06-16
 * Time:13:51
 */
public class App {
    public static void main(String[] args) {
//        ApplicationContext applicationContext =
//                new ClassPathXmlApplicationContext("spring-config.xml");
//        User user = (User) applicationContext.getBean("user");
//        user.sayHi();
        BeanFactory beanFactory = new XmlBeanFactory(new ClassPathResource("spring-config.xml"));
        //BeanFactory beanFactory1 = new ClassPathXmlApplicationContext("spring-config.xml");
        User user = (User) beanFactory.getBean("user");
        user.sayHi();

//        User user1 = beanFactory.getBean(User.class);
//        user1.sayHi();

        User user2 = beanFactory.getBean("user2",User.class);
        user2.sayHi();
        User user3 = beanFactory.getBean("user2",User.class);
        user3.sayHi();
        System.out.println(user2);
        System.out.println(user3);

//        User user1 = (User) beanFactory.getBean("user1");
//        user1.sayHi();
//
//        System.out.println(user);
//        System.out.println(user1);
    }
}
