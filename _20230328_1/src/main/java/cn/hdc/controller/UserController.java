package cn.hdc.controller;

import cn.hdc.dao.UserDao;
import cn.hdc.entity.User;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.util.List;
import java.util.Scanner;

public class UserController {
    public static void main(String[] args) {
        ApplicationContext context = new ClassPathXmlApplicationContext("application.xml");
        UserDao userDao = (UserDao) context.getBean("userDao");
        List<User> users = userDao.findAllUser();
        System.out.println("欢迎来到学生管理系统！");
        System.out.println("请输入用户名：");
        Scanner scanner = new Scanner(System.in);
        String username_scanner = scanner.nextLine();

        for (User user : users) {
            if (username_scanner.equals(user.getUsername())) {
                System.out.println("请输入" + user.getUsername() + "的密码：");
                String password_scanner = scanner.nextLine();
                if (password_scanner.equals(user.getPassword())) {
                    System.out.println("登录成功！");
                    System.out.println(user.getUsername() + "是" + user.getCourse() + "的");
                    return;
                } else {
                    System.out.println("用户名或者密码错误！");
                    return;
                }
            }
        }

    }
}
