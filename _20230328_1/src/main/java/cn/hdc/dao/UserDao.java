package cn.hdc.dao;

import cn.hdc.entity.User;

import java.util.List;

public interface UserDao {
    public List<User> findAllUser();
}
