import com.hdc.beans.*;
import com.hdc.beans.component.BeanLife;
import com.hdc.beans.controller.ArtController;
import com.hdc.beans.controller.StudentControllerA;
import com.hdc.beans.controller.StudentControllerB;
import com.hdc.model.Student;
import com.hdc.model.UserInfo;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class App {
    public static void main(String[] args) {
        ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("spring-config.xml");
        BeanLife beanLife = context.getBean(BeanLife.class);
        System.out.println("使用Bean");
        System.out.println("销毁Bean");
        context.destroy();
    }
}


//1.得到spring上下文对象
//ApplicationContext context = new ClassPathXmlApplicationContext("spring-config.xml");
//        //2.使用上下文对象.getBean()获取bean对象
//        UserController userController = context.getBean("userController", UserController.class);
//        //3.使用bean
//        userController.sayHi();
//
//        UserService userService = context.getBean("userService", UserService.class);
//        userService.sayHi("Java");
//
//        UserRepository userRepository = context.getBean("userRepository",UserRepository.class);
//        userRepository.sayHi();
//
//        UserComponent userComponent = context.getBean("userComponent", UserComponent.class);
//        userComponent.sayHi();
//
//        UserConfiguration userConfiguration = context.getBean("userConfiguration", UserConfiguration.class);
//        userConfiguration.sayHi();

//        UService uService = context.getBean("uService", UService.class);
//        uService.sayHi();
//
//        UService uService1 = context.getBean("UService", UService.class);
//        uService1.sayHi();
//        ApplicationContext context = new ClassPathXmlApplicationContext("spring-config.xml");
//        UserInfo userInfo = context.getBean("user", UserInfo.class);
//        System.out.println(userInfo);
//
//        UserInfo userInfo1 = context.getBean("userInfo", UserInfo.class);
//        System.out.println(userInfo1);
//        ApplicationContext context = new ClassPathXmlApplicationContext("spring-config.xml");
//        ArtController artController = context.getBean("artController", ArtController.class);
//        artController.addArt("标题", "正文");
//
//        ApplicationContext context = new ClassPathXmlApplicationContext("spring-config.xml");
//        StudentControllerA studentControllerA = context.getBean("studentControllerA", StudentControllerA.class);
//        Student studentA = studentControllerA.getStudent();
//        System.out.println("ControllerA :" + studentA);
//        System.out.println("");
//        StudentControllerB studentControllerB = context.getBean("studentControllerB", StudentControllerB.class);
//        Student studentB = studentControllerB.getStudent();
//        System.out.println("ControllerB :" + studentB);
//    }
//}
