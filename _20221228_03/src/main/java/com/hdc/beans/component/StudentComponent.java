package com.hdc.beans.component;

import com.hdc.model.Student;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component
public class StudentComponent {
//    @Scope("prototype")
    @Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
    @Bean(name = "student")
    public Student studentNo1() {
        Student student = new Student();
        student.setId(1);
        student.setName("悟空");
        student.setPassword("123456");
        student.setAge(10);
        return student;
    }

}
