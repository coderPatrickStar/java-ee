package com.hdc.beans.controller;

import com.hdc.beans.component.StudentComponent;
import com.hdc.model.Student;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

@Controller
public class StudentControllerA {
    @Autowired
    private Student student;

    public Student getStudent() {
        Student thisStudent = student;
        System.out.println("StudentControllerA 中 student ：" + thisStudent);
        System.out.println("");
        System.out.println("修改 student 信息");
        System.out.println("");
        //修改student信息
        thisStudent.setName("八戒");
        return thisStudent;
    }
}
