package com.hdc.beans.controller;

import com.hdc.beans.UserBeans;
import com.hdc.beans.service.ArtService;
import com.hdc.model.UserInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;

import javax.annotation.Resource;

@Controller
public class ArtController {

    //注入方式一：属性注入
//    @Autowired
//    private ArtService artService;

    //注入方式二：构造方法注入
//    private ArtService artService;
//
//    @Autowired
//    public ArtController(ArtService artService) {
//        this.artService = artService;
//    }

    //注入方式三：Setter注入
//    private ArtService artService;
//
//    @Autowired
//    public void setArtService(ArtService artService) {
//        this.artService = artService;
//    }
//    @Resource
//    private ArtService artService;
//
//    public void addArt(String title, String content) {
//        if (title != null && content != null && !title.equals("") && !content.equals("")) {
//            //传参正常
//            artService.addArt(title, content);
//        } else {
//            System.out.println("添加文章失败，前端传递了非法参数！");
//        }
//
//    }
    @Autowired
    @Qualifier(value = "user1")
    private UserInfo userInfo;

    public void addArt(String title, String content) {
        System.out.println(userInfo);
    }
}
