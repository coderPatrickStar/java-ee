package com.hdc.beans;

import com.hdc.model.UserInfo;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;

//特殊说明：@Bean 注解一定要配合类注解一起使用（否则就会导致对象不能正常的存储到spring容器
@Component
public class UserBeans {
    @Bean
    public UserInfo user() {
        UserInfo userInfo = new UserInfo();
        userInfo.setId(1);
        userInfo.setName("Java");
        userInfo.setPassword("123456");
        return userInfo;
    }

    @Bean
    public UserInfo user1() {
        UserInfo userInfo = new UserInfo();
        userInfo.setId(2);
        userInfo.setName("Mysql");
        userInfo.setPassword("123456");
        return userInfo;
    }
}
