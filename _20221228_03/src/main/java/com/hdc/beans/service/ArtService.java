package com.hdc.beans.service;

import org.springframework.stereotype.Service;

@Service
public class ArtService {
    public void addArt(String title, String content) {
        //调用后面的流程
        System.out.println("执行了文章的service方法  " + title + " " + content);
    }
}
