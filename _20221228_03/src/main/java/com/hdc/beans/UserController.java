package com.hdc.beans;

import org.springframework.stereotype.Controller;
//Controller = <bean id="user" class="com.hdc.beans.User></bean>
@Controller
public class UserController {
    public void sayHi() {
        System.out.println("UserController");
    }
}
