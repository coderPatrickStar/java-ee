package cn.hdc.pojo;

import java.util.List;

public class ClassInfo {
    private int cid;
    private String cname;
    private int sum;
    private List<TeachInfo> teachInfoList;

    @Override
    public String toString() {
        return "ClassInfo{" +
                "cid=" + cid +
                ", cname='" + cname + '\'' +
                ", sum=" + sum +
                ", teachInfoList=" + teachInfoList +
                '}';
    }

    public int getCid() {
        return cid;
    }

    public void setCid(int cid) {
        this.cid = cid;
    }

    public String getCname() {
        return cname;
    }

    public void setCname(String cname) {
        this.cname = cname;
    }

    public int getSum() {
        return sum;
    }

    public void setSum(int sum) {
        this.sum = sum;
    }

    public List<TeachInfo> getTeachInfoList() {
        return teachInfoList;
    }

    public void setTeachInfoList(List<TeachInfo> teachInfoList) {
        this.teachInfoList = teachInfoList;
    }
}
