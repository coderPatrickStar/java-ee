package cn.hdc.mapper;

import cn.hdc.pojo.TeachInfo;

public interface ClassInfoMapper {
    public TeachInfo findTeacher(Integer id);
}
