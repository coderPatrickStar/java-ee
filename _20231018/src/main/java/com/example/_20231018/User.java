package com.example._20231018;
import org.springframework.stereotype.Component;

@Component
public class User {
    public String name;
    public Integer age;

    @Override
    public String toString() {
        return "User{" +
                "name='" + name + '\'' +
                ", age=" + age +
                '}';
    }
}
