package com.example._20231018;

import com.example._20231018.controller.BookController;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;

@SpringBootApplication
public class Application {

    public static void main(String[] args) {
        ConfigurableApplicationContext ctx = SpringApplication.run(Application.class, args);
        BookController bean = ctx.getBean(BookController.class);
        System.out.println(bean);
        User user = ctx.getBean(User.class);
        System.out.println(user);
    }

}
