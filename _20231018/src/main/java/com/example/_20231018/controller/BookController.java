package com.example._20231018.controller;

import com.example._20231018.Book;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

//@Controller
//@ResponseBody
@RestController
@RequestMapping("/books")
public class BookController {
    //    @RequestMapping(method = RequestMethod.POST)
    //    @ResponseBody
    @PostMapping
    public String save(@RequestBody Book book) {
        System.out.println("book save..." + book);
        return "{'moudle':'book save'}";
    }

    //    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    //    @ResponseBody
    @DeleteMapping("/{id}")
    public String delete(@PathVariable Integer id) {
        System.out.println("book delete..." + id);
        return "{'moudle':'book delete'}";
    }

    //    @RequestMapping(method = RequestMethod.PUT)
    //    @ResponseBody
    @PutMapping
    public String update(@RequestBody Book book) {
        System.out.println("book update..." + book);
        return "{'moudle':'book update'}";
    }

    //    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    //    @ResponseBody
    @GetMapping("/{id}")
    public String getById(@PathVariable Integer id) {
        System.out.println("book getById..." + id);
        return "{'moudle':'book getById'}";
    }

    //    @RequestMapping(method = RequestMethod.GET)
    //    @ResponseBody
    @GetMapping
    public String getAll() {
        System.out.println("book getAll...");
        return "{'moudle':'book getAll'}";
    }
}
