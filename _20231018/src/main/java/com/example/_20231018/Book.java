package com.example._20231018;

import org.springframework.stereotype.Component;

@Component
public class Book {
    public String name;
    public int price;

    @Override
    public String toString() {
        return "Book{" +
                "name='" + name + '\'' +
                ", price=" + price +
                '}';
    }
}
