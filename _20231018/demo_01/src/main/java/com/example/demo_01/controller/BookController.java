package com.example.demo_01.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class BookController {
    @GetMapping("/books")
    public String getBooks() {
        System.out.println("springboot is running...");
        return "springboot is running";
    }
}
