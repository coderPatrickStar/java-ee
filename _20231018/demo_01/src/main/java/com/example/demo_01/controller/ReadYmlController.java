package com.example.demo_01.controller;

import com.example.demo_01.entity.MyDataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.env.Environment;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/read")
public class ReadYmlController {
    @Value("${country}")
    private String country_variable;

    @Value("${user.name_1}")
    private String name_variable;

    @Value("${user.age}")
    private Integer age_variable;

    @Value("${a.b.c.d.e}")
    private Integer e_variable;

    @Value("${likes[1]}")
    private String music_variable;

    @Value("${likes2[0]}")
    private String game_variable;

    @Value("${users3[0].name}")
    private String name111_variable;

    @Value("${tempDir}")
    private String temp_dir_variable;

    @Value("${tempDir1}")
    private String temp_dir1_variable;

    //使用自动装配将所有的数据封装到一个对象Environment中
    @Autowired
    private Environment environment;

    @Autowired
    private MyDataSource myDataSource;

    @GetMapping
    public String showVariable() {
        System.out.println(country_variable);
        System.out.println(name_variable);
        System.out.println(age_variable);
        System.out.println(e_variable);
        System.out.println(music_variable);
        System.out.println(game_variable);
        System.out.println(name111_variable);
        System.out.println(temp_dir_variable);
        System.out.println(temp_dir1_variable);
        System.out.println("---------------------------------");
        System.out.println(environment.getProperty("country"));
        System.out.println(environment.getProperty("user.age"));
        System.out.println("---------------------------------");
        System.out.println(myDataSource);

        return country_variable +
                name_variable +
                age_variable +
                e_variable +
                music_variable +
                game_variable +
                name111_variable +
                temp_dir_variable +
                temp_dir1_variable
                ;
    }
}
