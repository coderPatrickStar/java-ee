package com.example._20230112_1.mapper;

import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface UserMapper {
    public int addUser(String username,String password);
}
