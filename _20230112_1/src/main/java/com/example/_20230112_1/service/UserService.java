package com.example._20230112_1.service;

import com.example._20230112_1.mapper.SysLogMapper;
import com.example._20230112_1.mapper.UserMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

@Service
public class UserService {
    @Autowired
    private UserMapper userMapper;

    @Autowired
    private SysLogService sysLogService;

    @Transactional(propagation = Propagation.NESTED)
    public int addUser(String username, String password) {
        int result = userMapper.addUser(username, password);
        sysLogService.addLog("hello");
        return result;
    }
}
