package com.example._20230112_1.service;

import com.example._20230112_1.mapper.SysLogMapper;
import org.apache.ibatis.transaction.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.interceptor.TransactionAspectSupport;

@Service
public class SysLogService {
    @Autowired
    private SysLogMapper sysLogMapper;

    @Transactional(propagation = Propagation.NESTED)
    public int addLog(String desc) {
        int result = sysLogMapper.addLog(desc);
        try {
            int count = 10 / 0;
        } catch (Exception e) {
            //回滚当前事务
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
        }
        return result;
    }
}
