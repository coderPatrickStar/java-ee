package com.example._20230112_1.controller;

import com.example._20230112_1.service.SysLogService;
import com.example._20230112_1.service.UserService;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@ResponseBody
@RequestMapping("/user")
public class UserController {
    @Autowired
    private UserService userService;

    @Autowired
    private SysLogService sysLogService;

    @RequestMapping("/add")
    @Transactional
    public String addUser(String username, String password) {
        if (StringUtils.hasLength(username) && StringUtils.hasLength(password)) {
            userService.addUser(username, password);
            return "";
        }
        return "请填写参数！";
    }

//    @RequestMapping("/add")
//    @Transactional
//    public String addUser(String username, String password) {
//        if (StringUtils.hasLength(username) && StringUtils.hasLength(password)) {
//            userService.addUser(username, password);
//            sysLogService.addLog("syslog表中desc添加成功！");
//            return "";
//        }
//        return "请填写参数！";
//    }




}
