package com.example.demo.controller;

import com.example.demo.model.UserInfo;
import com.example.demo.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Creat by TanLei
 * Description:
 * User:86183
 * Date:2022-06-16
 * Time:13:51
 */
@RestController
public class UserController {

    @Autowired
    public UserService userService;

    @RequestMapping("/getuser")
    public UserInfo getUserById(Integer id){
        UserInfo userInfo = null;
        if(id!=null && id>0){
            userInfo = userService.getUserById(id);
        }
        return userInfo;
    }
}
