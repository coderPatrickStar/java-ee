package cn.hdc.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class DemoController {
    @RequestMapping("/demo1")
    public String demo1() {
        System.out.println("demo1 running");
        return "success.jsp";
    }

    @RequestMapping("/demo2")
    public String demo2() {
        System.out.println("demo2 running");
        return "success.jsp";
    }

    @RequestMapping("/uploadUrl")
    public String demo3(){

        return "main";
    }
}
