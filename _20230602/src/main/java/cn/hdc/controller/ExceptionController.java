package cn.hdc.controller;

import cn.hdc.exception.MyException;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;

@Controller
public class ExceptionController {
    @RequestMapping("/showNullPointer")
    public void showNullPointer() {
        ArrayList<String> list = null;
        list.add("hello");

    }

    @RequestMapping("showIO")
    public void showIO() throws IOException {
        FileInputStream fileInputStream = new FileInputStream("abc.txt");
    }

    @RequestMapping("showDefault")
    public void showDefault() {
        int i = 1 / 0;
    }

    @RequestMapping("/addData")
    public void addData() throws MyException {
        throw new MyException("新增数据异常");
    }
}
