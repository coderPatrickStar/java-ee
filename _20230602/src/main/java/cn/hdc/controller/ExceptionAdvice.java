package cn.hdc.controller;

import cn.hdc.exception.MyException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.ModelAndView;

//@ControllerAdvice
public class ExceptionAdvice {
    @ExceptionHandler(MyException.class)
    public ModelAndView doMyException(MyException e) {
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("error.jsp");
        modelAndView.addObject("msg", e.getMessage());
        return modelAndView;
    }

    @ExceptionHandler(Exception.class)
    public ModelAndView doOtherException(Exception e) {
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("error.jsp");
        modelAndView.addObject("msg", "网络异常");
        return modelAndView;
    }
}
