package cn.hdc.controller;

import cn.hdc.pojo.Resource;
import cn.hdc.utils.JSONFileUtils;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.commons.io.FileUtils;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import sun.misc.BASE64Encoder;

import javax.servlet.http.HttpServletRequest;
import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;

@Controller
public class FileController {
    @RequestMapping("/fileUpload")
    public String fileUpload(MultipartFile[] files, HttpServletRequest request) throws Exception {
        String path = request.getServletContext().getRealPath("/files/");
        System.out.println(path);
        ObjectMapper objectMapper = new ObjectMapper();
        //判断files为空，如果为空，上传失败，否则才开始进行上传的操作
        if (files != null && files.length > 0) {
            //开始文件上传操作
            // 文件名的集合
            List<Resource> list = new ArrayList<>();
            //遍历每个文件
            for (MultipartFile file : files) {
                //每个文件都需要保存，但是需要判断名字
                //判断文件是否重名，如果重名，做名字的重命名操作
                String filePath = path + "files.json";
                String fileNameJson = JSONFileUtils.readFile(filePath);
                System.out.println(fileNameJson);
                String filename = file.getOriginalFilename();
                System.out.println(filename);
                if (fileNameJson.length() > 0) {
                    //有其他名字，要判断名字
                    //先将json字符串转换成java对象集合
                    list = objectMapper.readValue(fileNameJson, new TypeReference<List<Resource>>() {
                    });
                    for (Resource resource : list) {
                        if (resource.getName().equals(filename)) {
                            String[] split = filename.split("\\.");
                            filename = split[0] + "(1)." + split[1];
                        }
                    }
                }

                //保存文件文件的位置应该是跟目录+改后的名字
                file.transferTo(new File(path + filename));
                //文件名信息要保存到files.json中
                list.add(new Resource(filename));
                //转换成json
                fileNameJson = objectMapper.writeValueAsString(list);
                // 写入到文件
                JSONFileUtils.writeFile(fileNameJson, path + "files.json");
            }
            request.setAttribute("msg", "上传成功！");
            return "forward:fileload.jsp";
        }

        request.setAttribute("msg", "上传失败！");
        return "forward:fileload.jsp";
    }

    @RequestMapping(value = "/getFilesName", produces = "text/html;charset=utf-8")
    @ResponseBody
    public String getFilesName(HttpServletRequest request) throws Exception {
        String path = request.getServletContext().getRealPath("/files/files.json");
        String fileNameJson = JSONFileUtils.readFile(path);
        return fileNameJson;
    }

    //解决下载文件时乱码
    public String getFileName(HttpServletRequest request, String filename) throws UnsupportedEncodingException {
        BASE64Encoder base64Encoder = new BASE64Encoder();
        String agent = request.getHeader("User-Agent");
        if (agent.contains("Firefox")) {
            filename = "=?UTF-8" + new String(base64Encoder.encode(filename.getBytes("UTF-8"))) + "?=";
        } else {
            filename = URLEncoder.encode(filename, "UTF-8");
        }
        return filename;
    }

    @RequestMapping("/download")
    public ResponseEntity<byte[]> fileDownload(HttpServletRequest request, String filename) throws IOException {
//        下载文件所在路径
        String path = request.getServletContext().getRealPath("/files/");
//        创建文件对象
        File file = new File(path + File.separator + filename);
//        设置消息头
        HttpHeaders httpHeaders = new HttpHeaders();
//        文件名乱码处理
        filename = getFileName(request, filename);
//        打开文件
        httpHeaders.setContentDispositionFormData("attachment", filename);
//        下载返回的文件数据
        httpHeaders.setContentType(MediaType.APPLICATION_OCTET_STREAM);
//        使用ResponseEntity对象封装返回下载数据
        return new ResponseEntity<byte[]>(FileUtils.readFileToByteArray(file), httpHeaders, HttpStatus.OK);
    }
}
