package cn.hdc.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class HelloController {
    @RequestMapping("/hello")
    public String sayHello() {
        System.out.println("handler执行了。。。");
        return "succ.jsp";
    }

    @RequestMapping("/exp")
    public String exp() {
        System.out.println(1 / 0);
        return "succ.jsp";
    }
}
