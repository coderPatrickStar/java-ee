package cn.hdc.controller;

import cn.hdc.pojo.User;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpSession;

@Controller
public class UserController {
    //    系统首页
    @RequestMapping("/main")
    public String main() {
        return "main";
    }

    //    登录界面
    @RequestMapping("/toLogin")
    public String toLogin() {
        return "login";
    }

    //    订单详情页面
    @RequestMapping("/orderinfo")
    public String toOrderInfo() {
        return "orderinfo";
    }

    //    登录界面
    @RequestMapping("/login")
    public String login(User user, Model model, HttpSession session) {
        String username = user.getUsername();
        String password = user.getPassword();
        if (username != null && "zs".equals(username) && password != null && "666".equals(password)) {
            session.setAttribute("user", user);
            return "redirect:main";
        } else {
            model.addAttribute("msg", "用户名或密码错误");
            return "login";
        }
    }

    //    退出成功，跳转到登录页面
    @RequestMapping("/logout")
    public String logOut(HttpSession session) {
        session.invalidate();
        return "login";
    }
}
