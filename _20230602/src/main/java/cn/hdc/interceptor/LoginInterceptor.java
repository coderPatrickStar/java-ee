package cn.hdc.interceptor;

import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

public class LoginInterceptor implements HandlerInterceptor {
    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        //针对登录和跳转到登录页面的请求进行放行
        String requestURI = request.getRequestURI();
        System.out.println("LoginInterceptor拦截了" + requestURI + "这个界面,接下来进行登录逻辑验证");
        if (requestURI.indexOf("/login") > 0 || requestURI.indexOf("/toLogin") > 0) {
            return true;
        }
        HttpSession session = request.getSession();
        Object user = session.getAttribute("user");
        if (user != null) {
            System.out.println("用户已经登录，可以访问其他页面");
            return true;
        }

        request.setAttribute("msg", "您还没有登录，请先登录");
        System.out.println("用户未登录，将访问login.jsp，重新登录");
        //此处视图解析器不生效。原生javaweb技术
        request.getRequestDispatcher("/WEB-INF/login.jsp").forward(request, response);
        return false;
    }

    @Override
    public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler, ModelAndView modelAndView) throws Exception {
        HandlerInterceptor.super.postHandle(request, response, handler, modelAndView);
    }

    @Override
    public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex) throws Exception {
        HandlerInterceptor.super.afterCompletion(request, response, handler, ex);
    }
}
