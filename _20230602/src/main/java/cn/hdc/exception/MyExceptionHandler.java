package cn.hdc.exception;

import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerExceptionResolver;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.io.Writer;

//@Component
public class MyExceptionHandler implements HandlerExceptionResolver {
    @Override
    public ModelAndView resolveException(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, Object o, Exception e) {
        String msg = "";
        if (e instanceof MyException) {
            msg = e.getMessage();
        } else {
            Writer out = new StringWriter();
            PrintWriter printWriter = new PrintWriter(out);
            e.printStackTrace(printWriter);
            msg = "网络异常";
        }
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("error.jsp");
        modelAndView.addObject("msg", msg);
        return modelAndView;
    }
}
