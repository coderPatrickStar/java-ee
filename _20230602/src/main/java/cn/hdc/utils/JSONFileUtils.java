package cn.hdc.utils;

import org.apache.commons.io.IOUtils;

import java.io.FileInputStream;
import java.io.FileOutputStream;

public class JSONFileUtils {
    public static String readFile(String filepath) throws Exception {
        FileInputStream fileInputStream = new FileInputStream(filepath);
        return IOUtils.toString(fileInputStream);
    }

    public static void writeFile(String data, String filepath) throws Exception {
        FileOutputStream fileOutputStream = new FileOutputStream(filepath);
        IOUtils.write(data, fileOutputStream);
    }
}
