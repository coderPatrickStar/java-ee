package cn.hdc.mapper;

import cn.hdc.pojo.Employee;
import cn.hdc.utils.MybatisUtils;
import org.apache.ibatis.session.SqlSession;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class EmployeeMapperTest {
    private SqlSession session;

    @Before
    public void setUp() throws Exception {
        this.session = MybatisUtils.getSession();
    }

    @After
    public void tearDown() throws Exception {
        this.session.close();
    }

    @Test
    public void findEmployeeById() {
        Employee employee = session.selectOne("cn.hdc.mapper.EmployeeMapper.findEmployeeById", 1);
        System.out.println(employee);
    }

    @Test
    public void addEmployee() {
        Employee employee = new Employee();
        employee.setName("赵六");
        employee.setAge(23);
        employee.setPosition("董事长");
        int ret = session.update("cn.hdc.mapper.EmployeeMapper.addEmployee", employee);
        if (ret > 0) {
            System.out.println("添加成功！");
        } else {
            System.out.println("添加失败！");
        }
        session.commit();
    }

    @Test
    public void updateEmployeeById() {
        Employee employee = new Employee();
        employee.setName("赵六");
        employee.setAge(56);
        employee.setPosition("员工");
        employee.setId(4);
        int ret = session.update("cn.hdc.mapper.EmployeeMapper.updateEmployeeById", employee);
        if (ret > 0) {
            System.out.println("修改成功！");
        } else {
            System.out.println("修改失败！");
        }
        session.commit();
    }

    @Test
    public void deleteEmployeeById() {
        int ret = session.delete("cn.hdc.mapper.EmployeeMapper.deleteEmployeeById", 4);
        if (ret > 0) {
            System.out.println("删除成功！");
        } else {
            System.out.println("删除失败！");
        }
        session.commit();
    }
}