package cn.hdc.mapper;

import cn.hdc.pojo.Employee;

public interface EmployeeMapper {
    public Employee findEmployeeById(Integer id);

    public Integer addEmployee(Employee employee);

    public Integer updateEmployeeById(Employee employee);

    public Integer deleteEmployeeById(Integer id);
}
